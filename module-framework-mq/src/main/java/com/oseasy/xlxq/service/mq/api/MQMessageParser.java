package com.oseasy.xlxq.service.mq.api;

import com.oseasy.xlxq.service.mq.exceptions.InvalidMQEventMessageException;

/**
 * Created by Tandy on 2016/7/23.
 */
public interface MQMessageParser {
    public MQEvent parse(String message) throws InvalidMQEventMessageException;
}
