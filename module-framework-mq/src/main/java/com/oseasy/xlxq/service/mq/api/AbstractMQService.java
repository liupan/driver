package com.oseasy.xlxq.service.mq.api;

import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.core.utils.UUIDGenerator;

import java.util.Date;

/**
 * Created by Tandy on 2016/7/23.
 */
public abstract class AbstractMQService implements MQService {
    @Override
    public void publish(AbstractMQEvent event) {
        if(StringUtil.isEmpty(event.getId())){
            event.setId(UUIDGenerator.uuid());
            event.setTimestamp(new Date().getTime());
        }
        publishEvent(event);
    }

    protected abstract void publishEvent(AbstractMQEvent event) ;
}
