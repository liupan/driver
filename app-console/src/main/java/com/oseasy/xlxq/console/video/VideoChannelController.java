package com.oseasy.xlxq.console.video;

import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.video.model.VideoChannel;
import com.oseasy.xlxq.service.api.video.model.VideoRoom;
import com.oseasy.xlxq.service.api.video.service.VideoChannelService;
import com.oseasy.xlxq.service.api.video.service.VideoRoomService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * 测试自动构建
 * Created by heckman on 2017/5/16.
 */

@RestController
@RequestMapping("/console/video_channel")
@SuppressWarnings("all")
public class VideoChannelController {

    private static final Logger logger = LoggerFactory.getLogger(VideoChannelController.class);

    @Autowired
    private VideoChannelService videoChannelService;

    @Autowired
    private VideoRoomService videoRoomService;

    /**
     * 后台管理展示直播频道接口
     *
     * @param request
     * @param param
     * @return
     */
    @RequestMapping(value = "/list")
    public RestResponse<Page<VideoChannel>> list(@RequestParam(defaultValue = "1") Integer pageNo,
                                                 @RequestParam(defaultValue = "20") Integer pageSize) {
        RestResponse<Page<VideoChannel>> restResponse = new RestResponse<Page<VideoChannel>>();
        try {
            String hql = "from VideoChannel obj ";
            hql = HqlUtil.addOrder(hql,"sortNo","desc");
            Page<VideoChannel> page = videoChannelService.pageList(hql,pageNo, pageSize);
            restResponse.setSuccess(true);
            restResponse.setData(page);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    /**
     * 直播频道保存接口(分为新增和修改)
     *
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResponse save(HttpServletRequest request, @RequestBody VideoChannel videoChannel) {
        RestResponse restResponse = new RestResponse();
        try {
            if (!StringUtils.isEmpty(videoChannel.getId())) {
                //修改操作
                videoChannel.setLastTime(new Date());
                videoChannelService.save(videoChannel);
            } else {
                //新增操作
                videoChannelService.save(videoChannel);
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    /**
     * 删除直播频道接口（删除之前要确保该直播频道下面没有直播房间，否则提示不能删除）
     *
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public RestResponse delete(HttpServletRequest request, @PathVariable String id) {
        RestResponse restResponse = new RestResponse();
        try {
            if (!StringUtils.isEmpty(id)) {
                List<VideoRoom> list = this.queryByChannelId(id);
                if (list.size() > 0) {
                    restResponse.setSuccess(false);
                    restResponse.setErrorCode(ErrorConstant.VIDEO_CHANNEL_DEL_EXPT_CODE);
                    restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.VIDEO_CHANNEL_DEL_EXPT_CODE));
                } else {
                    videoChannelService.delete(id);
                    restResponse.setSuccess(true);
                }
            } else {
                restResponse.setSuccess(false);
                restResponse.setErrorCode(ErrorConstant.PARAM_NULL_EXPT_CODE);
                restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.PARAM_NULL_EXPT_CODE));
            }
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    /**
     * 查询单个
     *
     * @return
     */
    @RequestMapping("/find/{id}")
    public RestResponse<VideoChannel> find(HttpServletRequest request, @PathVariable String id) {
        RestResponse<VideoChannel> restResponse = new RestResponse<VideoChannel>();
        try {
            if (!StringUtils.isEmpty(id)) {
                restResponse.setSuccess(true);
                restResponse.setData(videoChannelService.findById(id));
            } else {
                restResponse.setSuccess(false);
                restResponse.setErrorCode(ErrorConstant.PARAM_NULL_EXPT_CODE);
                restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.PARAM_NULL_EXPT_CODE));
            }
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    /**
     * 根据videoRoom中的videoChannelId查询
     *
     * @param videoChannelId
     * @return
     */
    public List<VideoRoom> queryByChannelId(String videoChannelId) {
        String hql = " from VideoRoom obj where obj.videoChannel.id = ? and obj.deleted = 0";
        Iterator<VideoRoom> iterator = videoRoomService.list(hql, videoChannelId).iterator();
        List<VideoRoom> list = new ArrayList<>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }


}
