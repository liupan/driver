package com.oseasy.xlxq.console.order;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.logistics.LogisticsService;
import com.oseasy.xlxq.service.api.order.model.ConfigExpress;
import com.oseasy.xlxq.service.api.order.model.OrderInfo;
import com.oseasy.xlxq.service.api.order.model.OrderLogistics;
import com.oseasy.xlxq.service.api.order.model.OrderStatus;
import com.oseasy.xlxq.service.api.order.service.ConfigExpressService;
import com.oseasy.xlxq.service.api.order.service.OrderInfoService;
import com.oseasy.xlxq.service.api.order.service.OrderLogisticsService;
import com.oseasy.xlxq.service.api.sys.service.TenantUserAddrsService;
import com.oseasy.xlxq.service.api.tenant.model.TenantUser;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by heckman on 2017/5/23.
 */

@RestController
@RequestMapping("/console/order_logistics")
public class OrderLogisticsController {

    private static final Logger logger = LoggerFactory.getLogger(OrderLogisticsController.class);

    @Autowired
    private OrderInfoService orderInfoService;

    @Autowired
    private TenantUserAddrsService tenantUserAddrsService;

    @Autowired
    private OrderLogisticsService orderLogisticsService;

    @Autowired
    private LogisticsService logisticsService;

    @Autowired
    private ConfigExpressService configExpressService;

    /**
     * 管理后台发货接口[POST]
     *
     * @return
     */
    @RequestMapping(value = "/deliver", method = RequestMethod.POST)
    public RestResponse deliver(@RequestBody OrderLogistics orderLogistics) {
        RestResponse restResponse = new RestResponse();
        try {
            OrderInfo orderInfo = orderInfoService.findById(orderLogistics.getOrderInfo().getId());
            TenantUser user = orderInfo.getTenantUser();

            System.out.println(JSONObject.toJSONString(orderInfo));

            //待发货状态下才能发货
            if (OrderStatus.PENDING_SHIP_CODE.equals(orderInfo.getOrderStatus())) {
                //填充订单物流信息对象
                orderLogistics.setDeliverStatus("1");
                orderLogistics.setTenantUser(user);
                orderLogistics.setOrderInfo(orderInfo);
                orderLogistics.setPurchaserName(orderInfo.getTenantUserAddrs().getConsignee());
                orderLogistics.setPurchaserTel(orderInfo.getTenantUserAddrs().getReceivingTel());
                orderLogistics.setOrderAddre(orderInfo.getTenantUserAddrs().getReceivingAddress());

                //将订单状态改为代收货
                orderInfo.setOrderStatus(OrderStatus.PENDING_RECEIVE_CODE);

                orderLogisticsService.save(orderLogistics);
                orderInfoService.save(orderInfo);

                restResponse.setSuccess(true);
            } else {
                restResponse.setErrorCode(ErrorConstant.CANNOT_DELIVER_ORDER_CODE);
                restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.CANNOT_DELIVER_ORDER_CODE));
            }
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * <p>
     * 查询订单物流流转信息（根据物流公司code和订单号查询）
     *
     * @param orderId
     * @return
     */
    @RequestMapping("/logistic_info/{orderId}")
    public RestResponse<JSONObject> query(@PathVariable String orderId) {
        RestResponse<JSONObject> restResponse = new RestResponse<JSONObject>();
        try {
            //根据订单id查询物流公司和订单号
            String hql = "from OrderLogistics obj ";
            hql = HqlUtil.addCondition(hql, "orderInfo.id", orderId);
            OrderLogistics orderLogistics = orderLogisticsService.findUnique(hql);

            //..................获取物流公司code
            String companyCode = this.getExpressCode(orderLogistics.getDeliverCompany());

            //调用物流接口查询物流信息
            String str = logisticsService.logistics(companyCode, orderLogistics.getDeliverNum());
            JSONObject data = JSONObject.parseObject(str);

            restResponse.setData(data);
            restResponse.setSuccess(true);

        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    public String getExpressCode(String companyName) {
        String hql = "from ConfigExpress obj ";
        hql = HqlUtil.addCondition(hql, "name", companyName);
        List<ConfigExpress> list = configExpressService.getList(hql);
        return list.get(0).getCode();
    }


}
