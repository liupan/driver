package com.oseasy.xlxq.console.login;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.exceptions.UserLoginExpiredException;
import com.oseasy.xlxq.service.api.merchant.model.Merchant;
import com.oseasy.xlxq.service.api.merchant.service.MerchantService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 登录处理器
 * Created by liups on 2016/7/6.
 */
@Controller
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private MerchantService merchantService;

    @RequestMapping("/login")
    @ResponseBody
    public RestResponse login(HttpServletRequest request, HttpServletResponse response,
                              String username, String pwd, String key) throws UserLoginExpiredException {
        RestResponse<JSONObject> restResponse = new RestResponse<JSONObject>();
        //JSONObject data = new JSONObject();
        try {
            if (!StringUtils.isEmpty(username)) {
                //restResponse.setData(videoRoom);

                String hql = "from Merchant obj ";
                hql = HqlUtil.addCondition(hql, "username", username);
                List<Merchant> list = merchantService.getList(hql);

                JSONObject data = JSONObject.parseObject(JSONObject.toJSONString(list.get(0)));
                restResponse.setData(data);

                restResponse.setSuccess(true);
            } else {
                restResponse.setSuccess(false);
                restResponse.setErrorCode(ErrorConstant.PARAM_NULL_EXPT_CODE);
                restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.PARAM_NULL_EXPT_CODE));
            }
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

}
