package com.oseasy.xlxq.console.video;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.video.model.VideoChannel;
import com.oseasy.xlxq.service.api.video.model.VideoProduct;
import com.oseasy.xlxq.service.api.video.model.VideoRoom;
import com.oseasy.xlxq.service.api.video.service.VideoChannelService;
import com.oseasy.xlxq.service.api.video.service.VideoProductService;
import com.oseasy.xlxq.service.api.video.service.VideoRoomService;
import com.oseasy.xlxq.service.core.utils.*;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by heckman on 2017/5/16.
 */

@RestController
@RequestMapping("/console/video_room")
@SuppressWarnings("all")
public class VideoRoomController {

    private static final Logger logger = LoggerFactory.getLogger(VideoRoomController.class);

    @Autowired
    private VideoRoomService videoRoomService;

    @Autowired
    private VideoChannelService videoChannelService;

    @Autowired
    private VideoProductService videoProductService;

    /**
     * 展示直播间列表接口
     *
     * @param request
     * @param param
     * @return
     */
    @RequestMapping(value = "/list")
    public RestResponse<Page<VideoRoom>> list(
            @RequestParam(defaultValue = "20") Integer pageSize,
            @RequestParam(defaultValue = "1") Integer pageNo,
            String roomname, String anchorname, String status) {
        RestResponse<Page<VideoRoom>> restResponse = new RestResponse<Page<VideoRoom>>();
        try {

            String hql = " from VideoRoom obj ";

            //设置过滤条件
            if (StringUtil.isNotBlank(status)) {
                hql = HqlUtil.addCondition(hql, "status", status);
            }
            /**
             * roomname、anchorname支持模糊搜索
             */
            //StringBuffer sf = new StringBuffer(hql);
            if (StringUtil.isNotBlank(roomname)) {
                //hql = HqlUtil.addCondition(hql, "roomname", roomname);
                hql = HqlUtil.addCondition(hql, "roomname", roomname, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            if (StringUtil.isNotBlank(anchorname)) {
                //hql = HqlUtil.addCondition(hql, "anchorname", anchorname);
                hql = HqlUtil.addCondition(hql, "anchorname", anchorname, HqlUtil.LOGIC_AND, HqlUtil.TYPE_STRING_LIKE, null);
            }
            hql = HqlUtil.addOrder(hql, "sortNo", "desc");
            Page<VideoRoom> page = videoRoomService.pageList(hql, pageNo, pageSize);

            restResponse.setSuccess(true);
            restResponse.setData(page);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 新增或修改直播间接口
     *
     * @param request
     * @param videoRoom
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResponse<VideoRoom> save(@RequestBody JSONObject param) {
        RestResponse<VideoRoom> restResponse = new RestResponse<VideoRoom>();
        try {
            VideoRoom videoRoom = JSONObject.toJavaObject(param.getJSONObject("videoRoom"), VideoRoom.class);
            JSONObject videoPro = param.getJSONObject("videoProduct");

            String ids = videoPro.getString("ids");
            String[] idArray = null;
            if (StringUtil.isNotBlank(ids)) {
                //有多个产品
                if (ids.contains(",")) {
                    idArray = ids.split(",");
                } else {
                    idArray = new String[]{ids};
                }
                videoRoom.setProductCount(idArray.length);
            }

            //做更新操作
            if (!StringUtils.isEmpty(videoRoom.getId())) {
                videoRoom.setLastTime(new Date());
                videoRoomService.save(videoRoom);
            } else {
                //新增操作
                videoRoomService.save(videoRoom);

                //查询出videoChannel
                VideoChannel videoChannel = videoChannelService.findById(videoRoom.getVideoChannel().getId());
                //将直播频道中的直播间数量加1
                videoChannel.setChaRmNum(videoChannel.getChaRmNum() + 1);
                videoChannelService.save(videoChannel);

            }


            if (StringUtil.isNotBlank(ids)) {
                if (idArray.length > 0) {
                    StringBuffer delHql = new StringBuffer("update tb_video_product set deleted=1 where charoom_id = '" + videoRoom.getId() + "' ");

                    videoProductService.updateNativeHql(delHql.toString());

                    String dateStr = DateUtils.formatDate(new Date(), "");
                    StringBuffer insertHql = new StringBuffer("insert into tb_video_product (id,charoom_id,product_id,create_dt,last_modi_dt,sortno,deleted,version) values ");
                    for (int i = 0; i < idArray.length; i++) {
                        if (i == idArray.length - 1) {
                            //最后一个
                            insertHql.append("('" + UUIDGenerator.uuid() + "','" + videoRoom.getId() + "','" + idArray[i] + "','" + dateStr + "','" + dateStr + "'," + new Date().getTime() + ",0,0)");
                        } else {
                            insertHql.append("('" + UUIDGenerator.uuid() + "','" + videoRoom.getId() + "','" + idArray[i] + "','" + dateStr + "','" + dateStr + "'," + new Date().getTime() + ",0,0),");
                        }
                    }
                    videoProductService.updateNativeHql(insertHql.toString());
                }
            }


            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    /**
     * 删除直播间接口(支持单个删除和批量删除)
     *
     * @param request
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete")
    public RestResponse delete(@RequestParam String ids) {
        RestResponse restResponse = new RestResponse();
        try {
            if (ids.contains(",")) {
                //批量删除
                String[] idArray = ids.split(",");
                for (String id : idArray) {
                    //删除直播间并把所属直播频道下的直播间数量减1
                    VideoRoom videoRoom = videoRoomService.findById(id);
                    if (videoRoom != null && videoRoom.getVideoChannel() != null) {
                        VideoChannel videoChannel = videoChannelService.findById(videoRoom.getVideoChannel().getId());

                        if (videoChannel.getChaRmNum() > 0) {
                            videoChannel.setChaRmNum(videoChannel.getChaRmNum() - 1);
                            videoChannelService.update(videoChannel.getId(), videoChannel);
                        }
                        videoRoomService.delete(id);
                    }
                }
            } else {
                //单个删除
                //删除直播间并把所属直播频道下的直播间数量减1
                VideoRoom videoRoom = videoRoomService.findById(ids);
                if (videoRoom != null && videoRoom.getVideoChannel() != null) {
                    VideoChannel videoChannel = videoChannelService.findById(videoRoom.getVideoChannel().getId());
                    if (videoChannel.getChaRmNum() > 0) {
                        videoChannel.setChaRmNum(videoChannel.getChaRmNum() - 1);
                        videoChannelService.update(videoChannel.getId(), videoChannel);
                    }
                    videoRoomService.delete(ids);
                }
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 根据id查询单个直播间接口
     *
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("/find/{id}")
    public RestResponse<JSONObject> find(@PathVariable String id) {
        RestResponse<JSONObject> restResponse = new RestResponse<JSONObject>();
        //JSONObject data = new JSONObject();
        try {
            if (!StringUtils.isEmpty(id)) {
                VideoRoom videoRoom = videoRoomService.findById(id);
                //restResponse.setData(videoRoom);

                String hql = "from VideoProduct obj ";
                hql = HqlUtil.addCondition(hql, "videoRoom.id", id);
                List<VideoProduct> list = videoProductService.getList(hql);

                JSONObject data = JSONObject.parseObject(JSONObject.toJSONString(videoRoom));

                data.put("videoProductList", list);
                restResponse.setData(data);

                restResponse.setSuccess(true);
            } else {
                restResponse.setSuccess(false);
                restResponse.setErrorCode(ErrorConstant.PARAM_NULL_EXPT_CODE);
                restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.PARAM_NULL_EXPT_CODE));
            }
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 直播间单个或批量上线接口
     *
     * @param request
     * @param paramList
     * @return
     */
    @RequestMapping("/on_line_set")
    public RestResponse onLineSet(@RequestParam String ids) {
        RestResponse restResponse = new RestResponse();
        try {
            //批量上线
            if (ids.contains(",")) {
                String[] idArray = ids.split(",");
                if (idArray.length > 0) {
                    videoRoomService.updateByArray("status", VideoRoom.ON_LINE_STATUS, "id", idArray);
                }
            } else {
                //单个上线
                VideoRoom videoRoom = new VideoRoom();
                videoRoom.setId(ids);
                videoRoom.setStatus(1);
                videoRoom.setLastTime(new Date());
                videoRoomService.update(videoRoom.getId(), videoRoom);
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 直播间批量下线接口
     *
     * @param request
     * @param paramList
     * @return
     */
    @RequestMapping("/off_line_set")
    public RestResponse offLineSet(@RequestParam String ids) {

        RestResponse restResponse = new RestResponse();
        try {
            if (ids.contains(",")) {
                //批量下线
                String[] idArray = ids.split(",");
                if (idArray.length > 0) {
                    videoRoomService.updateByArray("status", VideoRoom.OFF_LINE_STATUS, "id", idArray);
                }
            } else {
                //单个下线
                VideoRoom videoRoom = new VideoRoom();
                videoRoom.setId(ids);
                videoRoom.setStatus(0);
                videoRoom.setLastTime(new Date());
                videoRoomService.update(videoRoom.getId(), videoRoom);
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


}
