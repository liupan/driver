package com.oseasy.xlxq.console.tenant;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.console.comm.ConsoleConstants;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.tenant.model.TenantSpecialtyCatalog;
import com.oseasy.xlxq.service.api.tenant.service.TenantSpecialtyCatalogService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 土特产类别管理
 */
@RestController
@RequestMapping("/console/specialtycatalog")
public class TenantSpecialtyCatalogController extends AbstractPortalController {

    @Autowired
    private TenantSpecialtyCatalogService tenantSpecialtyCatalogService;

    /**
     * 获取实体信息
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public RestResponse getEntityById(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            TenantSpecialtyCatalog tenantSpecialtyCatalog = tenantSpecialtyCatalogService.findById(id);
            restResponse.setData(tenantSpecialtyCatalog);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 保存实体
     * @param
     * @return
     */
    @RequestMapping("/save")
    public RestResponse save(@RequestBody TenantSpecialtyCatalog tenantSpecialtyCatalog) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from TenantSpecialtyCatalog obj where obj.deleted=0 and obj.productName="+"'"+tenantSpecialtyCatalog.getProductName()+"'";
            TenantSpecialtyCatalog  tmp = (TenantSpecialtyCatalog)tenantSpecialtyCatalogService.getByHql(hql);
           String id = tenantSpecialtyCatalog.getId();
            if(StringUtil.isBlank(id)&&tmp!=null||id!=null&&!id.equals(tmp.getId())){
                logger.error("名称已经存在!");
                restResponse.setSuccess(false);
                restResponse.setErrorCode("0000");
                restResponse.setErrorMsg("该名称已经存在!");
                return restResponse;
            }

            if(StringUtil.isNotBlank(id)){
                tenantSpecialtyCatalogService.update(tenantSpecialtyCatalog.getId(),tenantSpecialtyCatalog);
            }else {
                tenantSpecialtyCatalogService.save(tenantSpecialtyCatalog);
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public RestResponse delete(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            tenantSpecialtyCatalogService.delete(id);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 查询列表
     * @param
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping("/list")
    public RestResponse getPageList(@RequestParam(defaultValue = "1") Integer pageNo,
                                    @RequestParam(defaultValue = "20") Integer pageSize) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from TenantSpecialtyCatalog obj";
           /* String moduleName = module.getModuleName();
            if (StringUtil.isNotBlank(moduleName)) {
                hql = HqlUtil.addCondition(hql, "moduleName", moduleName,null,"Like",null);
            }*/
            Page page = tenantSpecialtyCatalogService.pageList(hql,pageNo,pageSize);

            List<TenantSpecialtyCatalog> resultList = page.getResult();
            for(TenantSpecialtyCatalog tsc:resultList){
                String tscId = tsc.getId();
                String sql = "select count(1) from tb_tenant_specialty a where a.deleted = 0 and a.catalog_id ="+ "'" + tscId +"'";
                List<Object> rList = tenantSpecialtyCatalogService.getListBySql(sql);
                Object countObj = rList.get(0);
                //logger.info("========countObj s class type name"+countObj.getClass().getTypeName());
                int number = Integer.valueOf(String.valueOf(countObj));
               tsc.setNumber(number);
            }
            page.setResult(resultList);

            restResponse.setData(page);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 批量操作
     * @param
     * @return
     */
    @RequestMapping(value = "/batchoperate",method = RequestMethod.POST)
    public RestResponse batchoperate(@RequestBody JSONObject jsonObj) {
        RestResponse restResponse = new RestResponse();
        String ids =  jsonObj.getString("ids");
        String type =  jsonObj.getString("type");
        try {
            if (StringUtil.isNotBlank(ids)&&StringUtil.isNotBlank(type)) {
                String [] idArray = ids.split(",");
                if (ConsoleConstants.OPERATE_TYPE_DELETE.equals(type)) {
                    tenantSpecialtyCatalogService.updateByArray("deleted", ConsoleConstants.DELETE_FLAG, "id", idArray);
                }
            }else{
                return RestResponse.failed("0000","请求参数错误");
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 获取父级类别列表
     * @return
     */
    @RequestMapping("/parentslist")
    public RestResponse getParentsList() {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from TenantSpecialtyCatalog obj where obj.deleted=0 and obj.productLevel=0";
            List<TenantSpecialtyCatalog> rList = tenantSpecialtyCatalogService.getList(hql);
            restResponse.setData(rList);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     *
     * 土特产两级联动查询
     * @param productLevel 级别标识： 1：二级；0：一级
     * @param id 一级id
     * @return
     */
    @RequestMapping("/list2")
    public RestResponse list2(String productLevel,String id){
        RestResponse restResponse=new RestResponse();
        try{
            String hql = "from TenantSpecialtyCatalog obj ";
            if("0".equals(productLevel)){
                //获取一级类别
                hql = HqlUtil.addCondition(hql,"productLevel","0");
            }else if("1".equals(productLevel)){
                //获取二级类别
                hql = HqlUtil.addCondition(hql,"parentId",id);
                hql = HqlUtil.addCondition(hql,"productLevel","1");
            }
            List<TenantSpecialtyCatalog> list = tenantSpecialtyCatalogService.getList(hql);
            restResponse.setSuccess(true);
            restResponse.setData(list);
        }catch (Exception ex){
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return  restResponse;

    }

}
