package com.oseasy.xlxq.console.security;

import com.oseasy.xlxq.service.api.tenant.model.TenantAccount;
import com.oseasy.xlxq.service.api.tenant.service.TenantAccountService;
import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tandy on 2016/6/7.
 */
@Service("preUserDetailsService")
public class PreUserDetailsService implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

    public static final Logger logger = LoggerFactory.getLogger(PreUserDetailsService.class);
    @Autowired
    private RedisCacheService cacheManager;

    @Autowired
    private TenantAccountService tenantAccountService;
    private List<GrantedAuthority> roles(String... roles) {
        List<GrantedAuthority> authorities = new ArrayList<>(
                roles.length);
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority( role));
        }
        return authorities;
    }

    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
        if(logger.isDebugEnabled()){
            logger.debug("验证身份Token：{}",token);
        }
        String principal = (String) token.getPrincipal();
        User user = null;
        if(StringUtils.isNotBlank(principal)) {
            TenantAccount account = tenantAccountService.findAccountByUserName(principal);
            if(account != null) {
                user = new User(account.getUserName(),"",true,true,true,true,roles("ROLE_TENANT_USER"));
            }
        }
        if(user == null){
            throw new UsernameNotFoundException(principal);
        }
        if(logger.isDebugEnabled()){
            logger.debug("验证身份通过：{}->{}",token,user.getUsername());
        }
        return user;
    }

}
