package com.oseasy.xlxq.console.tenant;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.console.comm.ConsoleConstants;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.tenant.model.Tenant;
import com.oseasy.xlxq.service.api.tenant.model.TenantMerchant;
import com.oseasy.xlxq.service.api.tenant.service.TenantMerchantService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 商户管理
 */
@RestController
@RequestMapping("/console/merchant")
public class TenantMerchantController extends AbstractPortalController {

    @Autowired
    private TenantMerchantService tenantMerchantService;

    /**
     * 获取实体信息
     *
     * @param tenantMerchantId
     * @return
     */
    @RequestMapping("/{id}")
    public RestResponse getEntityById(@PathVariable("id") String tenantMerchantId) {
        RestResponse restResponse = new RestResponse();
        try {
            TenantMerchant tenantMerchant = tenantMerchantService.findById(tenantMerchantId);
            restResponse.setData(tenantMerchant);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 保存实体
     *
     * @param tenantMerchant
     * @return
     */
    @RequestMapping("/save")
    public RestResponse save(HttpServletRequest request, @RequestBody TenantMerchant tenantMerchant) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isNotBlank(tenantMerchant.getId())) {
                tenantMerchantService.save(tenantMerchant);
            } else {
                Tenant tenant = new Tenant();
                tenant.setId(getCurrentUser(request).getTenantId());
                tenantMerchant.setIsUse("0");
                tenantMerchant.setTenant(tenant);
                tenantMerchantService.save(tenantMerchant);
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    @RequestMapping("/delete/{id}")
    public RestResponse delete(@PathVariable("id") String tenantMerchantId) {
        RestResponse restResponse = new RestResponse();
        try {
            tenantMerchantService.delete(tenantMerchantId);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    @RequestMapping("/list")
    public RestResponse getPageList(@RequestParam(defaultValue = "1") Integer pageNo,
                                    @RequestParam(defaultValue = "20") Integer pageSize,
                                    String merchantName,
                                    String isUse) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from TenantMerchant obj";
           /* String merchantName = tenantMerchant.getMerchantName();
            if (StringUtil.isNotBlank(merchantName)) {
                hql = HqlUtil.addCondition(hql, "merchantName", merchantName,null,"Like",null);
            }*/

            if (StringUtil.isNotBlank(merchantName)) {
                hql = HqlUtil.addCondition(hql, "merchantName", merchantName, HqlUtil.LOGIC_AND,
                        HqlUtil.TYPE_STRING_LIKE, null);
            }
            if (StringUtil.isNotBlank(isUse)) {
                hql = HqlUtil.addCondition(hql, "isUse", isUse);
            }
            hql=HqlUtil.addOrder(hql,"createTime","desc");

            Page page = tenantMerchantService.pageList(hql, pageNo, pageSize);
            restResponse.setData(page);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 批量操作
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/batchoperate", method = RequestMethod.POST)
    public RestResponse batchoperate(@RequestBody JSONObject jsonObj) {
        RestResponse restResponse = new RestResponse();
        String ids = jsonObj.getString("ids");
        String type = jsonObj.getString("type");
        try {
            if (StringUtil.isNotBlank(ids) && StringUtil.isNotBlank(type)) {
                String[] idArray = ids.split(",");
                if (ConsoleConstants.OPERATE_TYPE_DELETE.equals(type)) {
                    tenantMerchantService.updateByArray("deleted", ConsoleConstants.DELETE_FLAG, "id", idArray);
                }
            } else {
                return RestResponse.failed("0000", "请求参数错误");
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 单个和批量启用
     *
     * @param ids
     * @return
     */
    @RequestMapping("/online")
    public RestResponse onLineSet(@RequestParam String ids) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isNotBlank(ids) && ids.contains(",")) {
                String[] idsArray = ids.split(",");
                tenantMerchantService.updateByArray("isUse", "1", "id", idsArray);
            } else if (StringUtil.isNotBlank(ids)) {
                String[] idsArray = new String[]{ids};
                tenantMerchantService.updateByArray("isUse", "1", "id", idsArray);
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

    /**
     * 单个或批量禁用接口
     *
     * @param ids
     * @return
     */
    @RequestMapping("/offline")
    public RestResponse offLineSet(@RequestParam String ids) {
        RestResponse restResponse = new RestResponse();
        try {
            if (StringUtil.isNotBlank(ids) && ids.contains(",")) {
                String[] idsArray = ids.split(",");
                tenantMerchantService.updateByArray("isUse", "0", "id", idsArray);
            } else if (StringUtil.isNotBlank(ids)) {
                String[] idsArray = new String[]{ids};
                tenantMerchantService.updateByArray("isUse", "0", "id", idsArray);
            }
            restResponse.setSuccess(true);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    @RequestMapping("/list2")
    public RestResponse<List<TenantMerchant>> selectList() {
        RestResponse<List<TenantMerchant>> restResponse = new RestResponse<List<TenantMerchant>>();
        try {
            String hql = "from TenantMerchant obj ";
            List<TenantMerchant> list = tenantMerchantService.getList(hql);
            restResponse.setSuccess(true);
            restResponse.setData(list);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }


    @RequestMapping(value = "/excel", method = RequestMethod.GET)
    public void exportExcel(HttpServletRequest request, HttpServletResponse response
            , String merchantName, String isUse) {

        String hql = "from TenantMerchant obj";
        if (StringUtil.isNotBlank(merchantName)) {
            hql = HqlUtil.addCondition(hql, "merchantName", merchantName, HqlUtil.LOGIC_AND,
                    HqlUtil.TYPE_STRING_LIKE, null);
        }
        if (StringUtil.isNotBlank(isUse)) {
            hql = HqlUtil.addCondition(hql, "isUse", isUse);
        }
        List<TenantMerchant> list = tenantMerchantService.getList(hql);
        String title = "商户";
        //lastTime
        String[] headers = new String[]{"商户名称", "经营范围", "联系人", "联系方式", "编辑时间", "状态"};
        String[] values = new String[]{"merchantName", "businessScope", "contactPerson", "contactTelephone", "lastTime", "isUse:1=上线;0=下线"};
        downloadExcel(title, null, headers, values, list, null, null, response);
    }

}
