package com.oseasy.xlxq.console.sys;

import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.console.comm.ConsoleConstants;
import com.oseasy.xlxq.service.api.sys.model.TenantUserWxExt;
import com.oseasy.xlxq.service.api.sys.model.TenantUserWxExtExcel;
import com.oseasy.xlxq.service.api.sys.service.TenantUserWxExtService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *用户微信属性
 */
@RestController
@RequestMapping("/console/userwxext")
public class TenantUserWxExtController extends AbstractPortalController {

    @Autowired
    private TenantUserWxExtService tenantUserWxExtService;

    /**
     * 获取实体信息
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public RestResponse getEntityById(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            TenantUserWxExt tenantUserWxExt = tenantUserWxExtService.findById(id);
            restResponse.setData(tenantUserWxExt);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 保存实体
     * @param
     * @return
     */
    @RequestMapping("/save")
    public RestResponse save(@RequestBody TenantUserWxExt tenantUserWxExt) {
        RestResponse restResponse = new RestResponse();
        try {
            if(StringUtil.isNotBlank(tenantUserWxExt.getId())){
                tenantUserWxExtService.update(tenantUserWxExt.getId(),tenantUserWxExt);
            }else {
                tenantUserWxExtService.save(tenantUserWxExt);
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("/delete/{id}")
    public RestResponse delete(@PathVariable("id") String id) {
        RestResponse restResponse = new RestResponse();
        try {
            tenantUserWxExtService.delete(id);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }


    /**
     * 查询列表
     * @param
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping("/list")
    public RestResponse getPageList(@RequestParam(defaultValue = "1") Integer pageNo,
                                    @RequestParam(defaultValue = "20") Integer pageSize,
                                    HttpServletRequest request) {
        RestResponse restResponse = new RestResponse();
        try {
            String hql = "from TenantUserWxExt obj";

            String name = request.getParameter("name");
            String sexMan = request.getParameter("sexMan");
            String sexWoman = request.getParameter("sexWoman");
            String sexUnknown = request.getParameter("sexUnknown");
            String phone = request.getParameter("phone");
            String timeStart = request.getParameter("timeStart");
            String timeEnd = request.getParameter("timeEnd");
            String followed = request.getParameter("followed");
            String concern = request.getParameter("concern");

            if (StringUtil.isNotBlank(name)) {
                hql = HqlUtil.addCondition(hql, "tenantUser.name", name,HqlUtil.LOGIC_AND,HqlUtil.TYPE_STRING_LIKE,null);
            }
            if (StringUtil.isNotBlank(sexMan)&&"1".equals(sexMan)) {
                hql = HqlUtil.addCondition(hql, "sexFlag", 1,HqlUtil.LOGIC_AND,HqlUtil.TYPE_NUMBER,HqlUtil.COMPARECHAR_EQ);
            }
            if (StringUtil.isNotBlank(sexWoman)&&"1".equals(sexWoman)) {
                hql = HqlUtil.addCondition(hql, "sexFlag", 2,HqlUtil.LOGIC_AND,HqlUtil.TYPE_NUMBER,HqlUtil.COMPARECHAR_EQ);
            }
            if (StringUtil.isNotBlank(sexUnknown)&&"1".equals(sexUnknown)) {
                hql = HqlUtil.addCondition(hql, "sexFlag", 0,HqlUtil.LOGIC_AND,HqlUtil.TYPE_NUMBER,HqlUtil.COMPARECHAR_EQ);
            }
            if (StringUtil.isNotBlank(phone)) {
                hql = HqlUtil.addCondition(hql, "tenantUser.telephone", phone,HqlUtil.LOGIC_AND,HqlUtil.TYPE_STRING_LIKE,null);
            }
            if (StringUtil.isNotBlank(timeStart)) {
                hql = HqlUtil.addCondition(hql, "createTime", timeStart,HqlUtil.LOGIC_AND,HqlUtil.TYPE_DATE,HqlUtil.COMPARECHAR_GREAT_EQ);
            }
            if (StringUtil.isNotBlank(timeEnd)) {
                hql = HqlUtil.addCondition(hql, "createTime", timeEnd,HqlUtil.LOGIC_AND,HqlUtil.TYPE_DATE,HqlUtil.COMPARECHAR_LESS_EQ);
            }
            if (StringUtil.isNotBlank(followed)&&"1".equals(followed)) {
                hql = HqlUtil.addCondition(hql, "subscribe", 1,HqlUtil.LOGIC_AND,HqlUtil.TYPE_NUMBER,HqlUtil.COMPARECHAR_EQ);
            }
            if (StringUtil.isNotBlank(concern)&&"1".equals(concern)) {
                hql = HqlUtil.addCondition(hql, "subscribe", 0,HqlUtil.LOGIC_AND,HqlUtil.TYPE_NUMBER,HqlUtil.COMPARECHAR_EQ);
            }
            Page page = tenantUserWxExtService.pageList(hql,pageNo,pageSize);
            restResponse.setData(page);
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    /**
     * 批量操作
     * @param
     * @return
     */
    @RequestMapping(value = "/batchoperate",method = RequestMethod.POST)
    public RestResponse batchoperate(@RequestBody JSONObject jsonObj) {
        RestResponse restResponse = new RestResponse();
        String ids =  jsonObj.getString("ids");
        String type =  jsonObj.getString("type");
        try {
            if (StringUtil.isNotBlank(ids)&&StringUtil.isNotBlank(type)) {
                String [] idArray = ids.split(",");
                if (ConsoleConstants.OPERATE_TYPE_DELETE.equals(type)) {
                    tenantUserWxExtService.updateByArray("deleted", ConsoleConstants.DELETE_FLAG, "id", idArray);
                }
            }else{
                return RestResponse.failed("0000","请求参数错误");
            }
            restResponse.setSuccess(true);
            restResponse.setErrorCode("1001");
            restResponse.setErrorMsg("操作成功");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            restResponse.setSuccess(false);
            restResponse.setErrorCode("0000");
            restResponse.setErrorMsg("操作异常");
        }
        return restResponse;
    }

    @RequestMapping("/excel")
    public void export(HttpServletRequest request,HttpServletResponse response) {
        try {
            String hql = "from TenantUserWxExt obj";

            String name = request.getParameter("name");
            String sexMan = request.getParameter("sexMan");
            String sexWoman = request.getParameter("sexWoman");
            String sexUnknown = request.getParameter("sexUnknown");
            String phone = request.getParameter("phone");
            String timeStart = request.getParameter("timeStart");
            String timeEnd = request.getParameter("timeEnd");
            String followed = request.getParameter("followed");
            String concern = request.getParameter("concern");

            if (StringUtil.isNotBlank(name)) {
                hql = HqlUtil.addCondition(hql, "tenantUser.name", name,HqlUtil.LOGIC_AND,HqlUtil.TYPE_STRING_LIKE,null);
            }
            if (StringUtil.isNotBlank(sexMan)&&"1".equals(sexMan)) {
                hql = HqlUtil.addCondition(hql, "sexFlag", 1,HqlUtil.LOGIC_AND,HqlUtil.TYPE_NUMBER,HqlUtil.COMPARECHAR_EQ);
            }
            if (StringUtil.isNotBlank(sexWoman)&&"1".equals(sexWoman)) {
                hql = HqlUtil.addCondition(hql, "sexFlag", 2,HqlUtil.LOGIC_AND,HqlUtil.TYPE_NUMBER,HqlUtil.COMPARECHAR_EQ);
            }
            if (StringUtil.isNotBlank(sexUnknown)&&"1".equals(sexUnknown)) {
                hql = HqlUtil.addCondition(hql, "sexFlag", 0,HqlUtil.LOGIC_AND,HqlUtil.TYPE_NUMBER,HqlUtil.COMPARECHAR_EQ);
            }
            if (StringUtil.isNotBlank(phone)) {
                hql = HqlUtil.addCondition(hql, "tenantUser.telephone", phone,HqlUtil.LOGIC_AND,HqlUtil.TYPE_STRING_LIKE,null);
            }
            if (StringUtil.isNotBlank(timeStart)) {
                hql = HqlUtil.addCondition(hql, "createTime", timeStart,HqlUtil.LOGIC_AND,HqlUtil.TYPE_DATE,HqlUtil.COMPARECHAR_GREAT_EQ);
            }
            if (StringUtil.isNotBlank(timeEnd)) {
                hql = HqlUtil.addCondition(hql, "createTime", timeEnd,HqlUtil.LOGIC_AND,HqlUtil.TYPE_DATE,HqlUtil.COMPARECHAR_LESS_EQ);
            }
            if (StringUtil.isNotBlank(followed)&&"1".equals(followed)) {
                hql = HqlUtil.addCondition(hql, "subscribe", 1,HqlUtil.LOGIC_AND,HqlUtil.TYPE_NUMBER,HqlUtil.COMPARECHAR_EQ);
            }
            if (StringUtil.isNotBlank(concern)&&"1".equals(concern)) {
                hql = HqlUtil.addCondition(hql, "subscribe", 0,HqlUtil.LOGIC_AND,HqlUtil.TYPE_NUMBER,HqlUtil.COMPARECHAR_EQ);
            }

            List<TenantUserWxExt> rList = tenantUserWxExtService.getList(hql);
            List<TenantUserWxExtExcel> list = new ArrayList<TenantUserWxExtExcel>();
            TenantUserWxExtExcel entity;
            for (TenantUserWxExt tmp : rList) {
                entity = new TenantUserWxExtExcel();
                entity.setWxNickName(tmp.getWxNickName());
                entity.setName(tmp.getTenantUser().getName());
                String sex = "";
                Integer sexFlag = tmp.getSexFlag();
                if(sexFlag==null){
                    sex="未知";
                }else if(1==sexFlag){
                    sex="男";
                }else if(2==sexFlag){
                    sex="女";
                }else{
                    sex="未知";
                }
                entity.setSex(sex);

                entity.setBirthDay(tmp.getTenantUser().getBirthday()==null?"":new SimpleDateFormat("yyyy-MM-dd").format(tmp.getTenantUser().getBirthday()));
                entity.setPhoneNumber(tmp.getTenantUser().getTelephone());
                entity.setRegisterDate(tmp.getRegistrationDate()==null?"":new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tmp.getRegistrationDate()));

                Integer subscribe =  tmp.getSubscribe();
                String attentionState = "";
                if(subscribe==null){
                    attentionState = "未知";
                }else if(subscribe==1){
                    attentionState = "已关注";
                }else if(subscribe==0){
                    attentionState = "未关注";
                }
                entity.setAttentionState(attentionState);

                list.add(entity);
            }
            String title = "微信用户";
            String[] headers = new String[]{"微信昵称","姓名", "性别", "生日", "手机号", "注册日期", "微信关注状态"};
            String[] values = new String[]{"wxNickName","name","sex", "birthDay", "phoneNumber", "registerDate", "attentionState"};
            downloadExcel(title, null, headers, values, list, null, null, response);
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
        }
    }

}
