package com.oseasy.xlxq.console.config;

import com.oseasy.xlxq.service.core.utils.PasswordUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Created by tandy on 16/9/21.
 * 暗码配置
 */

@Configuration
public class HideCodeConfig {


    @Bean
    @Profile("no")
    public String hideCode(){
        return "Lwf49fF34fsjtb";
    }

    public static void main(String[] args) {
        System.out.println(PasswordUtil.desdecode("7C867D78C72FA4D3"));
    }
}
