package com.oseasy.xlxq.console.statistics;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.statistic.model.DailySell;
import com.oseasy.xlxq.service.api.statistic.service.StatisticHomeStayDayService;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heckman on 2017/6/12.
 */

@RestController
@RequestMapping("/console/daily_statistics")
public class DailystatisticsController {

    public static final Logger logger = LoggerFactory.getLogger(DailystatisticsController.class);

    @Autowired
    private StatisticHomeStayDayService service;

    @RequestMapping("/list")
    public RestResponse list(String startTime, String endTime,
                             @RequestParam(defaultValue = "1") Integer pageNo,
                             @RequestParam(defaultValue = "20")Integer pageSize) {
        RestResponse restResponse=new RestResponse();
        try{
            String sql ="SELECT " +
                    "   baseta.datestr, " +
                    "   tm.merchant_name AS merchantName, " +
                    "   baseta.dailySellSum, " +
                    "   baseta.dailyRefundSum, " +
                    "   baseta.purSellSum "+
                    "FROM " +
                    "   ( " +
                    "      SELECT " +
                    "         tt.datestr AS datestr, " +
                    "         tt.merchant_id AS merchantName, " +
                    "         sum(tt.sale_money) AS dailySellSum, " +
                    "         sum(tt.refund_money) AS dailyRefundSum, " +
                    "         ( " +
                    "            sum(tt.sale_money) - sum(tt.refund_money) " +
                    "         ) AS purSellSum " +
                    "      FROM " +
                    "         ( " +
                    "            SELECT " +
                    "               CONCAT( " +
                    "                  obj.`year`, " +
                    "                  '-', " +
                    "                  obj.`month`, " +
                    "                  '-', " +
                    "                  obj.`day` " +
                    "               ) AS datestr, " +
                    "               obj.merchant_id, " +
                    "               obj.refund_money, " +
                    "               obj.sale_money " +
                    "            FROM " +
                    "               `tb_statistic_ticket_day` obj " +
                    "            WHERE " +
                    "               obj.deleted != 1 " +
                    "            UNION " +
                    "               SELECT " +
                    "                  CONCAT( " +
                    "                     obj.`year`, " +
                    "                     '-', " +
                    "                     obj.`month`, " +
                    "                     '-', " +
                    "                     obj.`day` " +
                    "                  ) AS datestr, " +
                    "                  obj.merchant_id, " +
                    "                  obj.refund_money, " +
                    "                  obj.sale_money " +
                    "               FROM " +
                    "                  `tb_statistic_specialty_day` obj " +
                    "               WHERE " +
                    "                  obj.deleted != 1 " +
                    "               UNION " +
                    "                  SELECT " +
                    "                     CONCAT( " +
                    "                        obj.`year`, " +
                    "                        '-', " +
                    "                        obj.`month`, " +
                    "                        '-', " +
                    "                        obj.`day` " +
                    "                     ) AS datestr, " +
                    "                     obj.merchant_id, " +
                    "                     obj.refund_money, " +
                    "                     obj.sale_money " +
                    "                  FROM " +
                    "                     `tb_statistic_homestay_day` obj " +
                    "                  WHERE " +
                    "                     obj.deleted != 1 " +
                    "         ) AS tt " +
                    "      GROUP BY " +
                    "         tt.datestr, " +
                    "         tt.merchant_id " +
                    "   ) AS baseta " +
                    "LEFT OUTER JOIN tb_tenant_merchant tm ON baseta.merchantName = tm.id";
            if(StringUtil.isNotBlank(startTime) && StringUtil.isBlank(endTime)){
                sql = sql + " HAVING STR_TO_DATE(baseta.datestr,'%Y-%m-%d') >= '"+startTime+"' ";
            }else if(StringUtil.isNotBlank(endTime) && StringUtil.isBlank(startTime)){
                sql = sql + " HAVING STR_TO_DATE(baseta.datestr,'%Y-%m-%d') <= '"+endTime+"' ";
            }else if(StringUtil.isNotBlank(startTime) && StringUtil.isNotBlank(endTime)){
                sql = sql + " HAVING STR_TO_DATE(baseta.datestr,'%Y-%m-%d') >= '"+startTime+"' and STR_TO_DATE(baseta.datestr,'%Y-%m-%d') <= '"+endTime+"' ";
            }

            String total_sql = "select count(1) from ( "+ sql +" ) as tt";
            String list_sql = "select * from ( "+ sql +" ) as tt limit "+(pageNo-1)*pageSize+","+pageSize;

            Long total = (service.executNativeTotalQuery(total_sql)).longValue();
            List list = service.executNativeQuery(list_sql, DailySell.class);

            List<DailySell> list1=new ArrayList<DailySell>();
            DailySell ds=null;
            for(int i=0;i<list.size();i++){
                ds = (DailySell)list.get(i);
                if(ds.getDailyRefundSum()==null){
                    ds.setDailyRefundSum(new BigDecimal(0));
                }else{
                    ds.setDailyRefundSum(ds.getDailyRefundSum().setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(ds.getPurSellSum()==null){
                    ds.setPurSellSum(ds.getDailySellSum().setScale(2,BigDecimal.ROUND_HALF_UP));
                }else{
                    ds.setPurSellSum(ds.getPurSellSum().setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                ds.setDailySellSum(ds.getDailySellSum().setScale(2,BigDecimal.ROUND_HALF_UP));
                list1.add(ds);
            }

            Page page=new Page((pageNo-1)*pageSize,total,pageSize,null);
            page.setResult(list1);

            System.out.println(JSONObject.toJSONString(page));
            restResponse.setData(page);
            restResponse.setSuccess(true);
        } catch (Exception ex){
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

}
