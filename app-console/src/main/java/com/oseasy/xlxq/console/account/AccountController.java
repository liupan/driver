package com.oseasy.xlxq.console.account;

import com.alibaba.fastjson.JSONObject;
import com.oseasy.xlxq.console.base.AbstractPortalController;
import com.oseasy.xlxq.service.api.exceptions.ErrorConstant;
import com.oseasy.xlxq.service.api.exceptions.UserLoginExpiredException;
import com.oseasy.xlxq.service.api.merchant.model.Merchant;
import com.oseasy.xlxq.service.api.merchant.service.MerchantService;
import com.oseasy.xlxq.service.api.tenant.service.TenantAccountService;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.StringUtil;
import com.oseasy.xlxq.service.mq.api.MQService;
import com.oseasy.xlxq.service.web.rest.RestResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 基本资料
 * Created by Tandy on 2016/6/8.
 */
@Controller
@RequestMapping("/account")
public class AccountController  extends AbstractPortalController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private MerchantService merchantService;

    @RequestMapping("/regist")
    @ResponseBody
    public RestResponse regist(@RequestBody Merchant merchant) throws UserLoginExpiredException {
        RestResponse<JSONObject> restResponse = new RestResponse<JSONObject>();
        try {
            if (!StringUtils.isEmpty(merchant.getUsername()) && !StringUtil.isEmpty(merchant.getPwd())) {
                //restResponse.setData(videoRoom);

                String hql = "from Merchant obj ";
                hql = HqlUtil.addCondition(hql, "username", merchant.getUsername());
                List<Merchant> list = merchantService.getList(hql);
                if (list.size() > 0) {
                    restResponse.setSuccess(false);
                    restResponse.setErrorMsg("用户名已存在");
                } else {
                    merchantService.save(merchant);
                    restResponse.setSuccess(true);
                }
            } else {
                restResponse.setSuccess(false);
                restResponse.setErrorCode(ErrorConstant.PARAM_NULL_EXPT_CODE);
                restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.PARAM_NULL_EXPT_CODE));
            }
        } catch (Exception ex) {
            logger.error("系统异常：", ex);
            restResponse.setSuccess(false);
            restResponse.setErrorCode(ErrorConstant.INNER_ERROR_CODE);
            restResponse.setErrorMsg(ErrorConstant.getErrMsg(ErrorConstant.INNER_ERROR_CODE));
        }
        return restResponse;
    }

}
