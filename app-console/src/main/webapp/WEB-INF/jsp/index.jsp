<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="/inc/metaLogin.jsp" %>
    <meta charset="UTF-8">
    <title>智慧乡村旅游管理平台</title>
    <link rel="stylesheet" href="${resPrefixUrl }/css/common.css">
    <link rel="stylesheet" href="${resPrefixUrl }/css/index.css">
</head>
<body>
<!-- 头部区域 -->
<div class="header">
    <!-- logo -->
    <div class="logo">乡丽乡亲管理平台</div>
    <!-- /logo -->

    <!-- 导航条 -->
    <div class="nav">
        <ul class="clearfix">
            <li class="active"><a href="#/home/summary"><span class="icon-summary">概要</span></a></li>
            <li><a href="#/video/dist"><span class="icon-video">视频</span></a></li>
            <li><a href="#/product/specialty/category"><span class="icon-product">商品</span></a></li>
            <li><a href="#/order/specialty"><span class="icon-order">订单</span></a></li>
            <li><a href="#/charts/specialty"><span class="icon-charts">数据</span></a></li>
            <li><a href="#/setting/business"><span class="icon-setting">设置</span></a></li>
        </ul>
    </div>
    <!-- /导航条 -->

    <!-- 用户信息 -->
    <div class="user">
        <!-- 用户账户名 -->
        <span class="user-name">admin</span>
        <!-- /用户账户名 -->
        <span class="icon-quit"></span>
    </div>
    <!-- /用户信息 -->
</div>
<!-- /头部区域 -->

<!-- 主体区域 -->
<div class="main">
    <!-- 侧边栏 -->
    <div class="sidebar">
        <!-- 概要 -->
        <ul class="active">
            <li><a href="#/home/summary">概要</a></li>
        </ul>
        <!-- 视频 -->
        <ul>
            <li><a href="#/video/dist">发布管理</a></li>
            <li><a href="#/video/channel">频道管理</a></li>
        </ul>
        <!-- 商品 -->
        <ul>
            <li><a href="#/product/specialty/category">土特产</a>
                <ul>
                    <li><a href="#/product/specialty/category">土特产类别管理</a></li>
                    <li><a href="#/product/specialty/add">土特产上架</a></li>
                </ul>
            </li>
            <li><a href="#/product/restaurant">民宿管理</a></li>
            <li><a href="#/product/ticket">门票管理</a></li>
            <li><a href="#/product/view">景点管理</a></li>
            <li><a href="#/product/wechatAd">微信广告栏</a></li>
        </ul>
        <!-- 订单 -->
        <ul>
            <li><a href="#/order/specialty">土特产订单</a></li>
            <li><a href="#/order/restaurant">民宿订单</a></li>
            <li><a href="#/order/ticket">门票订单</a></li>
            <li><a href="#/order/refundApply">退款申请</a></li>
            <li><a href="#/order/refundOrder">退款订单</a></li>
            <li><a href="#/order/saleDetails">日销售额明细</a></li>
        </ul>
        <!-- 数据 -->
        <ul>
            <li><a href="#/charts/specialty">土特产销售报表</a></li>
            <li><a href="#/charts/restaurant">民宿销售报表</a></li>
            <li><a href="#/charts/ticket">门票销售报表</a></li>
            <li><a href="#/charts/platform">平台用户数据</a></li>
            <li><a href="#/charts/wechat">微信访问数据</a></li>
        </ul>
        <!-- 设置 -->
        <ul>
            <li><a href="#/setting/business">商户管理</a></li>
            <li><a href="#/setting/user">用户管理</a></li>
            <li><a href="#/setting/role">角色权限管理</a></li>
            <li><a href="#/setting/wechat">微信用户</a></li>
            <li><a href="#/setting/log">操作日志</a></li>
            <li><a href="#/setting/feed">意见反馈</a></li>
            <li><a href="#/setting/wechatSetting">微信公众号配置</a></li>
            <li><a href="#/setting/message">短信配置</a></li>
        </ul>
    </div>

    <!-- 右侧区域 -->
    <div class="rightContent">
        <!-- 面包屑 -->
        <div class="breadcrumb clearfix"></div>

        <!-- 内容区域 -->
        <div class="clearfix" id="content"></div>
    </div>
</div>
<!-- /主体区域 -->

<!-- 脚本加载 -->
<script src="${resPrefixUrl }/bower_components/jquery/dist/jquery.min.js"></script>
<script src="${resPrefixUrl }/bower_components/art-template/dist/template.js"></script>
<script src="${resPrefixUrl }/js/common.js"></script>
<script src="${resPrefixUrl }/js/index.js"></script>
<!-- /脚本加载-->
</body>
</html>