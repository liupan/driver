<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="/inc/metaLogin.jsp" %>
    <meta charset="UTF-8">
    <!--设置浏览器的兼容模式版本（让IE使用最新的渲染引擎工作）-->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>乡丽乡亲管理平台</title>
    <link rel="shortcut icon" href="${resPrefixUrl }/img/favicon.ico">
    <link rel="stylesheet" href="${resPrefixUrl }/css/common.css">
    <link rel="stylesheet" href="${resPrefixUrl }/css/login.css">
</head>
<body>
<!-- 最外层容器 -->
<div class="container">
    <!-- 背景图 -->
    <img class="bg" src="${resPrefixUrl }/img/login.jpg">
    <!-- /背景图 -->

    <!-- /内容区域 -->
    <div class="content">
        <!-- 标题 -->
        <h3>乡丽乡亲<span class="red">管理平台</span></h3>
        <!-- /标题 -->

        <!-- 表单 -->
        <form id="loginForm" action="${ctx}/login" method="post">
       <%-- <div class="username">
            <label for="username">用户名</label><input id="username" name="username"  type="text"><span>用户名错误</span>
        </div>
        <div class="password">
            <label for="password">密&nbsp;&nbsp;&nbsp;码</label><input id="password" name="password"  type="password"><span>密码错误</span>
        </div>

           <div class="form-group form-block"   >
                <div class="col-md-6 remove-padding">
                    <input type="text" name="validateCode" placeholder="验证码" class="form-control" id="form-code">
                </div>
                <div class="col-md-6 remove-padding border">
                    <span class="code-img"><img src="${ctx}/vc/get?dt=${currentTime}" id="imgValidateCode"/></span>
                </div>
          </div>

       &lt;%&ndash; <div class="login"><a id="login">登录</a></div>&ndash;%&gt;
            <div class="login"><input type="submit" id="login" value="登录"></input></div>--%>

           <p class="username">
               <label for="username">用户名</label><input id="username" name="username" type="text">
           </p>
           <p class="password">
               <label for="password">密&nbsp;&nbsp;&nbsp;码</label><input id="password" name="password" type="password">
           </p>

           <!-- 验证码 -->
           <p class="validator">
               <span>验证码</span><input type="text" name="validateCode" id="validateCode" maxlength="4"><img
                   src="${ctx}/vc/get?dt=${currentTime}">
           </p>

           <p class="login"><input type="submit" value="登录"></p>
    </form>
        <!-- /表单 -->

            <div style="text-align: center;margin:5px auto">
                <c:if test="${not empty param.er}">
                    <c:if test="${param.er eq 'true'}"><a style="color: red" >${SPRING_SECURITY_LAST_EXCEPTION.message}</a></c:if>
                    <c:if test="${param.er eq 'vcer'}"><a style="color: red" >验证码错误</a></c:if>
                </c:if>

            </div>

        <!-- 脚注 -->
        <div class="foot">
            <p><a href="http://www.os-easy.com">噢易云</a></p>
            <p><a href="http://www.os-easy.com">www.os-easy.com</a></p>
        </div>
        <!-- /脚注 -->
    </div>
    <!-- /内容区域 -->
</div>
<!-- /最外层容器 -->

<!-- 脚本加载 -->
<%--<script src="${resPrefixUrl }/bower_components/jquery/dist/jquery.min.js"></script>--%>

<!--bootstrapvalidator-->
<%--<script src="${resPrefixUrl }/bower_components/bootstrapvalidator/dist/js/bootstrapValidator.min.js"></script>--%>

<%--<script src="${resPrefixUrl }/js/login.js"></script>--%>
</body>
</html>