/**
 * Created by knight on 2017/5/7.
 */
$(function () {
    // 身份验证
    // 用户名是否存在
    var usernameExist = false;
    // 全局缓存用户名
    var username = "";
    // 获取用户名元素
    var $username = $("#username");
    // 获取密码元素
    var $password = $("#password");
    // 获取登录元素
    var $login = $("#login");


    // 用户名表单失去焦点时判断用户名是否存在
    $username.on("blur", function () {
        // $.remoteData(); 用户名是否存在

        // 模拟用户名的验证
        if ($(this).val() === "admin") {
            // 用户名存在
            usernameExist = true;
            username = $(this).val();
            return;
        }

        // 用户名不存在并且不为空
        if ($(this).val() != "") {
            $(this).next().css("visibility", "visible");
        }

    }).on("focus", function () {
        // 获取焦点时密码错误提示信息隐藏
        $(this).next().css("visibility", "hidden");
    });


    // 点击登录按钮判断密码是否正确
 //   $login.on("click", authenticate);


    // 密码框获取焦点时错误信息提示不可见
    $password.on("focus", function () {
        $(this).next().css("visibility", "hidden");
    });

    // 登录校验
    function authenticate() {
        console.log("登录");
        $("#loginForm").submit;
        return;
        // 如果用户名不存在
        if (!usernameExist) {
            $username.next().css("visibility", "visible");
            return;
        }

        // $.remoteData(); 获取用户名所对应的密码
        var password = $password.val();

        // 模拟用户名和密码的验证
        if (username === "admin" && password === "admin") {
            // 重定向到 domain/index.html#html/index/summary.html
            window.location.href = "/console/index?" + "username=" + username + "#/index/summary";
        } else {
            // 提示密码错误
            $password.next().css("visibility", "visible");
        }
    }
});
