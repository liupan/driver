/**
 * Created by knight on 2017/5/6.
 */
/*
 $(function () {
 // 改变面包屑导航
 function changeBreadcrumb() {
 // 面包屑数据
 var breadcrumbData = [];
 // 面包屑结构
 var breadcrumbHtml = "";

 breadcrumbData[0] = $(".nav li.active>a").text();
 breadcrumbData[1] = $(".sidebar>ul.active>li.active>a").text() || "";
 breadcrumbData[2] = $(".sidebar>ul.active>li.active li.active>a").text() || "";

 // 生成面包屑结构
 for (var i = 0; i < breadcrumbData.length; i++) {
 if (breadcrumbData[i]) {
 breadcrumbHtml += "<li>" + breadcrumbData[i] + "</li>";
 }
 }

 // 挂载到.breadcrumb的ul中
 $(".breadcrumb>ul").html(breadcrumbHtml);
 }

 // 页面ready完成时加载一次面包屑
 changeBreadcrumb();

 // 侧边栏与导航条联动
 $(".nav>ul").on("click", "li", function () {
 // 导航条切换
 $(this).addClass("active").siblings().removeClass("active");
 // 侧边栏联动
 $(".sidebar>ul").eq($(this).index()).addClass("active").siblings().removeClass("active");
 // 改变面包屑
 changeBreadcrumb();
 });

 // 侧边栏切换
 $(".sidebar").on("click", "li", function () {
 // 侧边栏切换
 $(this).addClass("active").siblings().removeClass("active");
 // 改变面包屑
 changeBreadcrumb();
 });

 // 模拟数据
 var datas = [{
 "title": "昨日销售额",
 "data": [{
 "name": "土特产",
 "sale": 3889
 }, {
 "name": "名宿",
 "sale": 388990
 }, {
 "name": "门票",
 "sale": 3889000
 }]
 }, {
 "title": "昨日订单",
 "data": [{
 "name": "土特产",
 "sale": 456
 }, {
 "name": "名宿",
 "sale": 789
 }, {
 "name": "门票",
 "sale": 678
 }]
 }, {
 "title": "用户数",
 "data": [{
 "name": "昨日新增",
 "sale": 456
 }, {
 "name": "平台累计",
 "sale": 789
 }]
 }, {
 "title": "微信流量",
 "data": [{
 "name": "昨日PV",
 "sale": 456
 }, {
 "name": "昨日UV",
 "sale": 789
 }]
 }];

 $.ajax({
 url: "test.html",
 method: "get",
 dataType: "text",
 success: function (data) {
 $(".details").html(data);
 }
 });

 });*/
$(function () {
    // 重定向

    // 从地址栏中过滤出锚点参数即重定向过来时传递的用户名进行校验
    /*
     var username = $.queryParams().username;

     // 如果未登录则重定向到登录页
     if (!username) {
     window.location.href = "/login.html";
     return;
     }*/

    // 获取导航条
    var $nav = $(".nav");
    // 获取侧边栏sidebar
    var $sidebar = $(".sidebar");
    // 获取右侧内容区域
    var $rightContent = $(".rightContent");
    // 获取面包屑容器
    var $breadcrumb = $(".breadcrumb");
    // 获取内容页容器
    var $contentContainer = $("#content");
    // 获取首页的锚点参数
    var homeAnchorParam = $(".nav a:first").attr("href");


    // 导航条切换
    $nav.on("click", "a", function () {
        // 获取当前点击的a的父元素li在导航条中的索引
        var currentLiIndex = $(this).parent().index();
        // 获取导航条中当前点击的a的href属性中的锚点参数
        var currentAnchorHref = $(this).attr("href");
        // 根据导航条中当前点击的a的href属性的锚点参数得到获取侧边栏(多级菜单栏)中相对应的a的选择器
        var associatedAnchorSelector = ".sidebar a[href='" + currentAnchorHref + "']";
        // 根据选择器获取侧边栏中相对应的a(last()是因为上级菜单中的a与下级菜单中的第一个a的锚点参数相同)
        var $sidebarAssociatedAnchor = $(associatedAnchorSelector).last();


        // 若果是首页，隐藏多级菜单，右侧内容区域的paddingLeft为0
        if (currentAnchorHref == homeAnchorParam) {
            // 隐藏侧边栏
            $sidebar.css("display", "none");
            // 隐藏右侧内容区域中的面包屑
            $breadcrumb.css("display", "none");
            // 右侧内容区域padding-left、padding-top改变
            $rightContent.css({
                "paddingLeft": 70,
                "paddingTop": 40
            });
        } else {
            // 显示侧边栏
            $sidebar.css("display", "block");
            // 显示右侧内容区域中的面包屑
            $breadcrumb.css("display", "block");
            // 右侧内容区域padding-left、padding-top还原
            $rightContent.css({
                "paddingLeft": 348,
                "paddingTop": 0
            });
        }

        // 切换选中的li
        $(this).parent().addClass("active").siblings().removeClass("active");

        // 触发侧边栏中相对应a的单击事件——模拟点击了多级菜单中对应的a，进而获取内容页的相关脚本及数据
        $sidebarAssociatedAnchor.trigger("click");

        // 切换侧边栏——侧边栏中一级菜单ul的索引为当前li在导航条中索引的ul显示
        $sidebar.children().eq(currentLiIndex).addClass("active").siblings().removeClass("active");
    });

    // 多级菜单的处理
    $sidebar.on("click", "a", function () {
        // 绑定内容页
        $.bindContent($contentContainer, $(this));

        // 生成面包屑
        $.breadcrumb($breadcrumb, $(this), $nav);

        // 切换多级菜单中的选中项
        // 当前a元素高亮
        $(this).addClass("current")
            // 当前a的兄弟元素ul展开(如果有)
            .siblings("ul").slideToggle("fast")
            // 返回到当前a的调用
            .end()
            // 当前a的父元素li
            .parent()
            // 当前a的父元素li的兄弟li
            .siblings()
            // 当前a的父元素li的兄弟li的子菜单ul关闭(如果有)
            .children("ul").slideUp("fast");

        // 菜单栏中所有其他a还原成原来的背景色
        $sidebar.find("a").not(this).removeClass("current");
    });


    // 页面加载完成时加载内容页summary.html作为首页
    $nav.find("li.active>a").trigger("click");

});