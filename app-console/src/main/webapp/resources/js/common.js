/**
 * Created by knight on 2017/5/7.
 */

/**
 * 扩展$类，添加一些工具方法
 */
;(function (window, document, $) {
    // 网页相关资源配置
    $.resource = $.resource || {
            api: {},
            // 面包屑中英文映射
            breadcrumbMap: {
                home: "概要",
                summary: "概要",
                video: "视频",
                dist: "视频发布",
                channel: "频道管理",
                product: "商品",
                specialty: "土特产",
                category: "土特产类别管理",
                add: "土特产上架",
                restaurant: "民宿管理",
                ticket: "门票管理",
                view: "景点管理",
                wechatAd: "微信广告栏",
                order: "订单"
            }
        };

    // 网页定制插件
    $.plugins = $.plugins || {
            /**
             * 分页插件类
             * @param {number} currentPage
             * @param {number} totalPage
             * @param {number} showCount
             */
            pagination: ""
        }


    /**
     * 获取location中当前ulr所包含的查询参数键值对对象
     * @type {*|Function}
     * @return {Object} paramsObj 查询参数所组成的键值对对象
     */
    $.queryParams = $.queryParams || function () {
            // 用于存放查询参数键值对的空对象
            var paramsObj = {};
            // 去掉查询参数前面的"?"
            var paramsStr = window.location.search.substr(1);
            // 按"&"符号分隔各对查询参数并保存为一个数组
            var paramsArr = paramsStr.split("&");

            for (var i = 0; i < paramsArr.length; i++) {
                // 每条查询参数键值对
                var param = paramsArr[i];
                // "="的索引
                var index = param.indexOf("=");
                // 查询参数的键
                var paramKey = param.substring(0, index);
                // 查询参数的值
                var paramVal = param.substr(index + 1);
                // 以键值对的形式添加到paramsObj对象中
                paramsObj[paramKey] = paramVal;
            }

            return paramsObj;
        };

    /**
     * 获取a标签href属性中的锚点参数(使用锚点参数实现路由)
     * @type {Function}
     * @param {Element} $element a元素
     * @return {String} 锚点参数去除"#"和第一个"/"后的字符串
     */
    $.getRouter = $.getRouter || function ($element) {
            var sharpIndex = $element[0].href.indexOf("#");
            if (sharpIndex == -1) {
                return "";
            }
            return $element[0].href.substr(sharpIndex + 2);
        };

    /**
     * 面包屑
     * @type {Function}
     * @param {Element} $container 面包屑的容器
     * @param {Element} $element a元素
     * @param {Element} [$nav] 与多级菜单联动的导航条，可选
     */
    $.breadcrumb = $.breadcrumb || function ($container, $element, $nav) {
            // 构成面包屑的所有a元素
            var $breadcrumbAnchors = $element.parents("li").children("a");
            // 转换成真数组——缓存下这些a元素，避免append()过程中对a元素数组的影响
            var breadcrumbAnchors = Array.prototype.slice.call($breadcrumbAnchors);
            // 初始化面包屑ul
            var $breadcrumb = $("<ul></ul>");

            // 生成面包屑的结构
            $.each(breadcrumbAnchors, function () {
                var $currentAnchor = $(this).clone();
                // 创建一个li
                $("<li></li>")
                // 将当前a元素追加到新创建的li元素中
                    .append($currentAnchor)
                    // 将追加了a元素的li元素追加到面包屑ul中
                    .prependTo($breadcrumb);
            });

            // 若果传入了与多级菜单联动的导航条，则将导航条中当前选中的li追加到面包屑中
            if ($nav) {
                // 获取导航条中当前选中的li
                var $navLi = $nav.find("li.active").clone();
                $navLi.prependTo($breadcrumb);
            }
            // 将导航条中当前选中的li追加到面包屑ul中

            // 将面包屑ul，即：$breadcrumb添加到$container容器中
            $container.empty().append($breadcrumb);
        };

    /**
     * 根据锚点参数将相应内容页所对应的html文件及js文件中的内容挂载到home.html中
     * @type {Function}
     * @param {Element} $container 内容页的容器
     * @param {Element} $element a元素
     */
    $.bindContent = $.bindContent || function ($container, $element) {
            // 获取锚点参数
            var router = $.getRouter($element);
            // 获取内容页所对应的html文件的相对路径
            var contentHtml = "/html/" + router + ".html";
            // 获取内容页所对应的js文件的相对路径
            var contentJs = "/js/" + router + ".js";
            // 获取锚点参数由"/"分隔后组成的数组
            var routerArray = router.split("/");
            // 获取js文件名，并作为该js脚本的id
            var jsID = "_" + routerArray[routerArray.length - 1];

            // 发送ajax请求该内容页所对应的html文件和js文件
            $.ajax({
                url: contentHtml,
                type: "GET",
                dataType: "text",
                success: function (data) {
                    // 将内容页所对应的html文件中的内容挂载到$container中
                    $container.html(data);

                    // 获取该内容页所对应的js脚本
                    var $script = $("#" + jsID);

                    // 如果存在这个脚本
                    if ($script.length != 0) {
                        $script.remove();
                    }

                    // 发送ajax请求该内容页所对应的js文件
                    $.ajax({
                        url: contentJs,
                        type: "GET",
                        dataType: "text",
                        success: function (data) {
                            // 将内容页所对应的js文件中的内容追加到body中
                            $("<script>" + data + "</script>")
                                .attr("id", jsID)
                                .appendTo($(document.body));
                        },
                        error: function () {
                            throw new Error("内容页的：" + contentJs + "请求出错!");
                        }
                    });

                },
                error: function () {
                    throw new Error("内容页的：" + contentHtml + "请求出错!");
                }
            });

        };

    /**
     * 根据url和查询参数发送ajax请求获取相应数据
     * @type {Function}
     * @param {String} url 服务器页面地址
     * @param {Object} params 本次请求所携带的参数
     * @return {Object} data json对象
     */
    $.remoteData = $.remoteData || function (url, params) {
            $.ajax({
                url: url,
                type: "POST",
                data: params,
                dataType: "json",
                success: function (data) {
                    return data;
                },
                error: function () {
                    throw new Error("向：" + url + "页面的ajax请求失败!参数对象为：" + params);
                }
            })
        };

    /**
     * 将远程获取到的数据渲染到artTemplate模板中，并将模板的结果添加到容器中
     * @type {Function}
     * @params {String} templateID 模板ID
     * @params {Json} dataObj json数据对象
     * @params {Element} $container 容器
     */
    $.renderData = $.renderData || function (templateID, dataObj, $container) {
            var html = template(templateID, {data: dataObj});
            $container.html(html);
        };


})(window, document, $);