package com.oseasy.xlxq.service.order.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.OrderLogistics;
import com.oseasy.xlxq.service.api.order.service.OrderLogisticsService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.order.dao.OrderLogisticsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class OrderLogisticsServiceImpl extends AbstractService<OrderLogistics> implements OrderLogisticsService {

    @Autowired
    private OrderLogisticsDao orderLogisticsDao;

    @Override
    public BaseDaoInterface<OrderLogistics, Serializable> getDao() {
        return orderLogisticsDao;
    }


}
