package com.oseasy.xlxq.service.sys.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.TenantUserAddrs;

import java.io.Serializable;

/**
 *
 */
public interface TenantUserAddrsDao extends BaseDaoInterface<TenantUserAddrs, Serializable> {
}
