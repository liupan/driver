package com.oseasy.xlxq.service.product.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;

import com.oseasy.xlxq.service.api.product.model.ProductWriteOff;

import java.io.Serializable;

/**
 *
 */
public interface ProductWriteOffDao extends BaseDaoInterface<ProductWriteOff, Serializable> {
}
