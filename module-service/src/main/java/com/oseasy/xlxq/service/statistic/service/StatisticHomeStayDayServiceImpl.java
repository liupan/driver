package com.oseasy.xlxq.service.statistic.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.statistic.model.StatisticHomeStayDay;
import com.oseasy.xlxq.service.api.statistic.service.StatisticHomeStayDayService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.statistic.dao.StatisticHomeStayDayDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
@Service
public class StatisticHomeStayDayServiceImpl extends AbstractService<StatisticHomeStayDay> implements StatisticHomeStayDayService {


    @Autowired
    private StatisticHomeStayDayDao statisticHomeStayDayDao;

    @Override
    public BaseDaoInterface<StatisticHomeStayDay, Serializable> getDao() {
        return statisticHomeStayDayDao;
    }

}
