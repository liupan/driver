package com.oseasy.xlxq.service.order.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.Cart;

import java.io.Serializable;

/**
 *
 */
public interface CartDao extends BaseDaoInterface<Cart, Serializable> {
}
