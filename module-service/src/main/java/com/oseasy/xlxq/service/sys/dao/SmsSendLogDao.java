package com.oseasy.xlxq.service.sys.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.SmsSendLog;

import java.io.Serializable;

/**
 *
 */
public interface SmsSendLogDao extends BaseDaoInterface<SmsSendLog, Serializable> {
}
