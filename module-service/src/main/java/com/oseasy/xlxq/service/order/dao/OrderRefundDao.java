package com.oseasy.xlxq.service.order.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.OrderRefund;

import java.io.Serializable;

/**
 *
 */
public interface OrderRefundDao extends BaseDaoInterface<OrderRefund, Serializable> {
}
