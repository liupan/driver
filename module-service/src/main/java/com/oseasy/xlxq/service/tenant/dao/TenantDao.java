package com.oseasy.xlxq.service.tenant.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.Tenant;

import java.io.Serializable;

/**
 * Created by liups on 2016/6/29.
 */
public interface TenantDao extends BaseDaoInterface<Tenant, Serializable> {

}
