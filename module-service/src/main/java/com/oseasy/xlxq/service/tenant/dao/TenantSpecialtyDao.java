package com.oseasy.xlxq.service.tenant.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantSpecialty;

import java.io.Serializable;

/**
 *
 */
public interface TenantSpecialtyDao extends BaseDaoInterface<TenantSpecialty, Serializable> {
}
