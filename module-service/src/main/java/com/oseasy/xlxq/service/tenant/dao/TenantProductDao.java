package com.oseasy.xlxq.service.tenant.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;

import java.io.Serializable;

/**
 *
 */
public interface TenantProductDao extends BaseDaoInterface<TenantProduct, Serializable> {
}
