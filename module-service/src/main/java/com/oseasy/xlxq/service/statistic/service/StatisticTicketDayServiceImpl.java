package com.oseasy.xlxq.service.statistic.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;

import com.oseasy.xlxq.service.api.statistic.model.StatisticTicketDay;
import com.oseasy.xlxq.service.api.statistic.service.StatisticTicketDayService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.statistic.dao.StatisticTicketDayDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class StatisticTicketDayServiceImpl extends AbstractService<StatisticTicketDay> implements StatisticTicketDayService {

    @Autowired
    private StatisticTicketDayDao statisticTicketDayDao;

    @Override
    public BaseDaoInterface<StatisticTicketDay, Serializable> getDao() {
        return statisticTicketDayDao;
    }

}
