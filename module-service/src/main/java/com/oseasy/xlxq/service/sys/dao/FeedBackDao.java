package com.oseasy.xlxq.service.sys.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.FeedBack;

import java.io.Serializable;

/**
 *
 */
public interface FeedBackDao extends BaseDaoInterface<FeedBack, Serializable> {
}
