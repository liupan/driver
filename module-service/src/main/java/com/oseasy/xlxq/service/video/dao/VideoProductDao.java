package com.oseasy.xlxq.service.video.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;

import com.oseasy.xlxq.service.api.video.model.VideoProduct;

import java.io.Serializable;

/**
 *
 */
public interface VideoProductDao extends BaseDaoInterface<VideoProduct, Serializable> {
}
