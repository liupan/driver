package com.oseasy.xlxq.service.merchant.dao;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.merchant.model.Merchant;

import java.io.Serializable;

public interface MerchantDao extends BaseDaoInterface<Merchant, Serializable> {
}
