package com.oseasy.xlxq.service.statistic.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.statistic.model.StatisticTenantUserDay;

import java.io.Serializable;

/**
 *
 */
public interface StatisticTenantUserDayDao extends BaseDaoInterface<StatisticTenantUserDay, Serializable> {
}
