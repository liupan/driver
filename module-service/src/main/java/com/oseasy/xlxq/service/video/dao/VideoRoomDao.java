package com.oseasy.xlxq.service.video.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.video.model.VideoRoom;

import java.io.Serializable;

/**
 *
 */
public interface VideoRoomDao extends BaseDaoInterface<VideoRoom, Serializable> {
}
