package com.oseasy.xlxq.service.tenant.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantUser;

import java.io.Serializable;

/**
 * Created by Tandy on 2016/6/24.
 */
public interface TenantUserDao extends BaseDaoInterface<TenantUser, Serializable> {

}
