package com.oseasy.xlxq.service.sms.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sms.model.SMSSendLog;
import com.oseasy.xlxq.service.api.sms.service.SMSSendLogService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.core.utils.Page;
import com.oseasy.xlxq.service.sms.dao.SMSSendLogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by Tandy on 2016/6/24.
 */
@Service
public class SMSSendLogServiceImpl extends AbstractService<SMSSendLog> implements SMSSendLogService {

    @Autowired
    private SMSSendLogDao smsSendLogDao;

    @Override
    public BaseDaoInterface<SMSSendLog, Serializable> getDao() {
        return smsSendLogDao;
    }

    @Override
    public Page<SMSSendLog> findByMobile(String mobile, int pageNo, int pageSize) {
        String hql = "from ProductEvaluate obj where obj.sendTo=?1 order by obj.createTime desc";
        return this.findByCustom(hql,true,pageNo,pageSize,mobile);
    }
}
