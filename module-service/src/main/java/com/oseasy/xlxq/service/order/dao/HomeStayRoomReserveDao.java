package com.oseasy.xlxq.service.order.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.HomeStayRoomReserve;

import java.io.Serializable;

/**
 *
 */
public interface HomeStayRoomReserveDao extends BaseDaoInterface<HomeStayRoomReserve, Serializable> {
}
