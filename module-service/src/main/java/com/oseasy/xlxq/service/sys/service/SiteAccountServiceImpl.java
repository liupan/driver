package com.oseasy.xlxq.service.sys.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.SiteAccount;
import com.oseasy.xlxq.service.api.sys.service.SiteAccountService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.sys.dao.SiteAccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class SiteAccountServiceImpl extends AbstractService<SiteAccount> implements SiteAccountService {

    @Autowired
    private SiteAccountDao siteAccountDao;

    @Override
    public BaseDaoInterface<SiteAccount, Serializable> getDao() {
        return siteAccountDao;
    }

}
