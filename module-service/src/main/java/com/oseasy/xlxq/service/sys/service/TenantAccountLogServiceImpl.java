package com.oseasy.xlxq.service.sys.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.TenantAccountLog;
import com.oseasy.xlxq.service.api.sys.service.TenantAccountLogService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.sys.dao.TenantAccountLogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class TenantAccountLogServiceImpl extends AbstractService<TenantAccountLog> implements TenantAccountLogService {

    @Autowired
    private TenantAccountLogDao tenantAccountLogDao;

    @Override
    public BaseDaoInterface<TenantAccountLog, Serializable> getDao() {
        return tenantAccountLogDao;
    }


}
