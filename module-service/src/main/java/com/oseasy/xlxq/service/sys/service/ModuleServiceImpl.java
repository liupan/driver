package com.oseasy.xlxq.service.sys.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;

import com.oseasy.xlxq.service.api.sys.model.Module;
import com.oseasy.xlxq.service.api.sys.service.ModuleService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.sys.dao.ModuleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 */
@Service
public class ModuleServiceImpl extends AbstractService<Module> implements ModuleService {

    @Autowired
    private ModuleDao moduleDao;

    @Override
    public BaseDaoInterface<Module, Serializable> getDao() {
        return moduleDao;
    }


    @Override
    public List<Map<String, Object>> getModuleByParent(List<Module> parentsList, List<Module> allModuleList) {
       //logger.info("===开始根据父菜单数组查询===");
        List<Map<String, Object>> rList = new ArrayList<Map<String, Object>>();
        Map<String, Boolean> stateMap = new HashMap<String, Boolean>();
         /* opened    : boolean  //展开
        disabled  : boolean  //禁用
        selected  : boolean  //选中*/
        stateMap.put("opened", false);//不展开
        stateMap.put("disabled", false);//不禁用
        stateMap.put("selected", false);//不选中
        for (Module pm : parentsList) {
            //logger.info("循环中的pm:"+pm);
            Map<String, Object> tMap = new HashMap<String, Object>();
            tMap.put("id", pm.getId());
            tMap.put("text", pm.getModuleName());
            tMap.put("icon", "none");
            tMap.put("state", stateMap);

            List<Module> chilrenList = new ArrayList<Module>();
            for (Module cm : allModuleList) {
                if (pm.getId().equals(cm.getParentId())) {
                    chilrenList.add(cm);
                }
            }

            if (chilrenList.size() > 0) {
                //logger.info("子菜单不为空");
                List<Map<String, Object>> cmList = new ArrayList<Map<String, Object>>();
              /*  for (Module tcm:chilrenList){
                    Map<String,Object> tcMap = new HashMap<String,Object>();
                    tcMap.put("id",tcm.getId());
                    tcMap.put("text",tcm.getModuleName());
                    tcMap.put("icon","none");
                    tcMap.put("state",stateMap);
                    cmList.add(tcMap);
                }*/

                cmList = getModuleByParent(chilrenList, allModuleList);
                //logger.info("cmList:"+cmList.toString());
                tMap.put("children", cmList);
            }

            rList.add(tMap);
        }

        return rList;
    }

    @Override
    public List<Module> getAllModuleList() {
        String hql = "from Module obj where obj.deleted = 0";
        List<Module> allMenuList = (List<Module>) this.list(hql);
        return allMenuList;
    }

    @Override
    public List<Module> getFirstMouleList(List<Module> allModuleList) {
        List<Module> firstModuleList = new ArrayList<Module>();
        if (allModuleList != null && allModuleList.size() > 0) {
            for (Module xm : allModuleList) {
                if ("1".equals(xm.getParentId())) {
                    firstModuleList.add(xm);
                }
            }
        }
        return firstModuleList;
    }


    public Map<String,Object> getMenuListByParentId(String parentId,int i){
        Map<String,Object> rMap = new HashMap<String,Object>();
        String hql = "from Module obj where obj.deleted = 0 and obj.parentId = " + "'" + parentId + "'" +" order by obj.sortNo asc";
        List<Module> menuList =  (List<Module>) this.list(hql);
        List<Object> menuIdList = new ArrayList<Object>();
       // List<Object> menuNameList = new ArrayList<Object>();
        List<Object> menuMapList = new ArrayList<Object>();
        if(menuList!=null&&menuList.size()>0) {
            for (Module m : menuList){
                menuIdList.add(m.getId());
               // menuNameList.add(m.getModuleName());
                Map<String,Object> tMap = new HashMap<String,Object>();
                tMap.put("id",m.getId());
                tMap.put("menuName",m.getModuleName());
                tMap.put("menuUrl",m.getModuleUrl());
                menuMapList.add(tMap);
            }
        }
        rMap.put("idListLevel" + i, menuIdList);
       // rMap.put("nameListLevel" + i, menuNameList);
        rMap.put("level"+i,menuMapList);
        return rMap;
    }

    @Override
    public Map<String,Object> getMenuListByParentIdList(List<String> parentIdList,int i){
        // logger.info("父id数组长度为："+parentIdList.size());
        Map<String,Object> rMap = new HashMap<String,Object>();
        List<List<String>> idList = new ArrayList<List<String>>();
      //  List<List<String>> nameList = new ArrayList<List<String>>();
        List<List<Map<String,Object>>> mapList = new ArrayList<List<Map<String,Object>>>();
        if(parentIdList!=null&&parentIdList.size()>0){
            for(String pId:parentIdList){
                Map<String,Object> mMap = getMenuListByParentId(pId,i);
                idList.add((List<String>)mMap.get("idListLevel"+i));
            //    nameList.add((List<String>)mMap.get("nameListLevel"+i));
                mapList.add((List<Map<String,Object>>)mMap.get("level"+i));
            }
        }else{
            idList.add(new ArrayList<String>());
        //    nameList.add(new ArrayList<String>());
            mapList.add(new ArrayList<>());
        }
        rMap.put("idListLevel"+i,idList);
      //  rMap.put("nameListLevel"+i,nameList);
        rMap.put("level"+i,mapList);
        //  logger.info("rMap.get(\"idListLevel\" + i).toString()"+rMap.get("idListLevel" + i).toString());
        return rMap;
    }


    @Override
    public List<Map<String, Object>> getModuleByParent2(List<Module> parentsList, List<Module> allModuleList, List<String> mIdList) {
//logger.info("===开始根据父菜单数组查询===");
        List<Map<String, Object>> rList = new ArrayList<Map<String, Object>>();
       // Map<String, Boolean> stateMap = new HashMap<String, Boolean>();
         /* opened    : boolean  //展开
        disabled  : boolean  //禁用
        selected  : boolean  //选中*/
       // stateMap.put("opened", false);//不展开
       // stateMap.put("disabled", false);//不禁用
       // stateMap.put("selected", false);//不选中
        for (Module pm : parentsList) {
            //logger.info("循环中的pm:"+pm);
            Map<String, Object> tMap = new HashMap<String, Object>();
            Map<String, Boolean>  stateMap = new HashMap<String, Boolean>();
            tMap.put("id", pm.getId());
            tMap.put("text", pm.getModuleName());
            tMap.put("icon", "none");
            stateMap.put("opened", false);//不展开
            stateMap.put("disabled", false);//不禁用
            if(mIdList.contains(pm.getId())){
                stateMap.put("selected", true);//选中
            }else{
                stateMap.put("selected", false);//不选中
            }
            tMap.put("state", stateMap);

            List<Module> chilrenList = new ArrayList<Module>();
            for (Module cm : allModuleList) {
                if (pm.getId().equals(cm.getParentId())) {
                    chilrenList.add(cm);
                }
            }

            if (chilrenList.size() > 0) {
                //logger.info("子菜单不为空");
                List<Map<String, Object>> cmList = new ArrayList<Map<String, Object>>();
              /*  for (Module tcm:chilrenList){
                    Map<String,Object> tcMap = new HashMap<String,Object>();
                    tcMap.put("id",tcm.getId());
                    tcMap.put("text",tcm.getModuleName());
                    tcMap.put("icon","none");
                    tcMap.put("state",stateMap);
                    cmList.add(tcMap);
                }*/

                cmList = getModuleByParent(chilrenList, allModuleList);
                //logger.info("cmList:"+cmList.toString());
                tMap.put("children", cmList);
            }

            rList.add(tMap);
        }

        return rList;
    }
}