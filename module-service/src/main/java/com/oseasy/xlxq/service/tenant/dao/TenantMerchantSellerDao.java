package com.oseasy.xlxq.service.tenant.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.tenant.model.TenantMerchantSeller;

import java.io.Serializable;

/**
 *
 */
public interface TenantMerchantSellerDao extends BaseDaoInterface<TenantMerchantSeller, Serializable> {
}
