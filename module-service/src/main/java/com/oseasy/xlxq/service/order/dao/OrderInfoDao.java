package com.oseasy.xlxq.service.order.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.OrderInfo;

import java.io.Serializable;

/**
 *
 */
public interface OrderInfoDao extends BaseDaoInterface<OrderInfo, Serializable> {
}
