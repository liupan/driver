package com.oseasy.xlxq.service.product.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;

import com.oseasy.xlxq.service.api.product.model.ProductEvaluate;
import com.oseasy.xlxq.service.api.product.service.ProductEvaluateService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.product.dao.ProductEvaluateDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class ProductEvaluateServiceImpl extends AbstractService<ProductEvaluate> implements ProductEvaluateService {

    @Autowired
    private ProductEvaluateDao productEvaluateDao;

    @Override
    public BaseDaoInterface<ProductEvaluate, Serializable> getDao() {
        return productEvaluateDao;
    }


}
