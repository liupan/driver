package com.oseasy.xlxq.service.sys.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.SiteAccount;

import java.io.Serializable;

/**
 *
 */
public interface SiteAccountDao extends BaseDaoInterface<SiteAccount, Serializable> {
}
