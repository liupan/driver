package com.oseasy.xlxq.service.sys.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.sys.model.Resources;

import java.io.Serializable;

/**
 *
 */
public interface ResourcesDao extends BaseDaoInterface<Resources, Serializable> {
}
