package com.oseasy.xlxq.service.order.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.Cart;
import com.oseasy.xlxq.service.api.order.service.CartService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.order.dao.CartDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class CartServiceImpl extends AbstractService<Cart> implements CartService {

    @Autowired
    private CartDao cartDao;

    @Override
    public BaseDaoInterface<Cart, Serializable> getDao() {
        return cartDao;
    }


}
