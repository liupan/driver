package com.oseasy.xlxq.service.order.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.OrderProduct;
import com.oseasy.xlxq.service.api.order.service.OrderProductService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.order.dao.OrderProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class OrderProductServiceImpl extends AbstractService<OrderProduct> implements OrderProductService {

    @Autowired
    private OrderProductDao orderProductDao;

    @Override
    public BaseDaoInterface<OrderProduct, Serializable> getDao() {
        return orderProductDao;
    }


}
