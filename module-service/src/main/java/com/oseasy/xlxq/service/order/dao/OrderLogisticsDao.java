package com.oseasy.xlxq.service.order.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.order.model.OrderLogistics;

import java.io.Serializable;

/**
 *
 */
public interface OrderLogisticsDao extends BaseDaoInterface<OrderLogistics, Serializable> {
}
