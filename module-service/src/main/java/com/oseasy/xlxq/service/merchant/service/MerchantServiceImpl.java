package com.oseasy.xlxq.service.merchant.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.merchant.model.Merchant;
import com.oseasy.xlxq.service.api.merchant.service.MerchantService;
import com.oseasy.xlxq.service.base.NewAbstractService;
import com.oseasy.xlxq.service.merchant.dao.MerchantDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public class MerchantServiceImpl extends NewAbstractService<Merchant> implements MerchantService {

    @Autowired
    private MerchantDao merchantDao;

    @Override
    public BaseDaoInterface<Merchant, Serializable> getDao() {
        return merchantDao;
    }

}
