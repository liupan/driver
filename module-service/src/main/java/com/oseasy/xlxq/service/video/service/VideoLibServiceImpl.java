package com.oseasy.xlxq.service.video.service;

import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.video.model.VideoLib;
import com.oseasy.xlxq.service.api.video.service.VideoLibService;
import com.oseasy.xlxq.service.base.AbstractService;
import com.oseasy.xlxq.service.video.dao.VideoLibDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 */
@Service
public class VideoLibServiceImpl extends AbstractService<VideoLib> implements VideoLibService {

    @Autowired
    private VideoLibDao videoLibDao;

    @Override
    public BaseDaoInterface<VideoLib, Serializable> getDao() {
        return videoLibDao;
    }


}
