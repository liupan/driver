package com.oseasy.xlxq.service.video.dao;


import com.oseasy.xlxq.service.api.base.BaseDaoInterface;
import com.oseasy.xlxq.service.api.video.model.VideoLib;

import java.io.Serializable;

/**
 *
 */
public interface VideoLibDao extends BaseDaoInterface<VideoLib, Serializable> {
}
