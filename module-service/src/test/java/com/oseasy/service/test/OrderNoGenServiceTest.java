package com.oseasy.service.test;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.api.order.service.OrderNoGenService;
import com.oseasy.xlxq.service.api.tenant.service.TenantProductService;
import com.oseasy.xlxq.service.api.tenant.service.TenantUserService;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.config.Constants;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import com.oseasy.xlxq.service.core.utils.Page;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tandy on 2016/7/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Import(value={ServiceConfig.class, ServiceApiConfig.class, FrameworkCacheConfig.class})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
@SpringApplicationConfiguration(OrderNoGenServiceTest.class)
public class OrderNoGenServiceTest {

    static {
        //将 spring boot 的默认配置文件设置为系统配置文件
        System.setProperty("spring.config.location","classpath:"+ Constants.DEFAULT_CONFIG_FILE);
    }


    @Bean
    public String getSystemId(){
        return "aa";
    }

    @Autowired
    private OrderNoGenService orderNoGenService;

    @Test
    public void test001(){
        System.out.println(orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK));
        System.out.println(orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK));
        System.out.println(orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK));
        System.out.println(orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK));
        System.out.println(orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK));
        System.out.println(orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK));
        System.out.println(orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK));
        System.out.println(orderNoGenService.genOrderNo(OrderNoGenService.ORDER_TYPE_FK));

    }

}
