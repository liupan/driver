package com.oseasy.service.test;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.api.tenant.model.TenantMerchant;
import com.oseasy.xlxq.service.api.tenant.service.TenantMerchantService;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.config.Constants;
import com.oseasy.xlxq.service.core.utils.ExportExcel;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by Tandy on 2016/7/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Import(value={ServiceConfig.class, ServiceApiConfig.class, FrameworkCacheConfig.class})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
@SpringApplicationConfiguration(AccountServiceTest.class)
public class ExcelDownloadTest {

    static {
        //将 spring boot 的默认配置文件设置为系统配置文件
        System.setProperty("spring.config.location","classpath:"+ Constants.DEFAULT_CONFIG_FILE);
    }

    @Autowired
    private TenantMerchantService tenantMerchantService;


    @Test
    public void test001() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, IOException {
        List<TenantMerchant> list = (List) tenantMerchantService.list();
        Assert.assertTrue(list.size() > 0);
        String title = "title";
        String[] headers =  new String[]{"商户名称","经营范围","联系人","联系方式","状态"};
        String[] values = new String[]{"merchantName","businessScope","contactPerson","contactTelephone","isUse:1=上线;0=下线"};
        HSSFWorkbook book = ExportExcel.exportExcel(title,"ONE",headers,values,list,"",null);
        book.write(new File("/tmp/a.xls"));
    }

}
