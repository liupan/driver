package com.oseasy.service.test;

import com.oseasy.xlxq.service.ServiceConfig;
import com.oseasy.xlxq.service.api.ServiceApiConfig;
import com.oseasy.xlxq.service.api.order.model.OrderProduct;
import com.oseasy.xlxq.service.api.order.service.OrderInfoService;
import com.oseasy.xlxq.service.api.order.service.OrderProductService;
import com.oseasy.xlxq.service.api.tenant.service.TenantProductService;
import com.oseasy.xlxq.service.cache.FrameworkCacheConfig;
import com.oseasy.xlxq.service.config.Constants;
import com.oseasy.xlxq.service.core.utils.HqlUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by heckman on 2017/5/24.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@Import(value = {ServiceConfig.class, ServiceApiConfig.class, FrameworkCacheConfig.class})
@EnableJpaRepositories(value = {"com.oseasy"})
@EnableAutoConfiguration
@SpringApplicationConfiguration(OrderInfoTest.class)
public class OrderInfoTest {

    static {
        //将 spring boot 的默认配置文件设置为系统配置文件
        System.setProperty("spring.config.location", "classpath:" + Constants.DEFAULT_CONFIG_FILE);
    }

    @Bean
    public String getSystemId() {
        return "aa";
    }

    @Autowired
    private OrderInfoService orderInfoService;


    @Autowired
    private OrderProductService orderProductService;

    @Autowired
    private TenantProductService tenantProductService;


    @Test
    public void test() {
        try {
            //String hql = "from OrderInfo oi left join OrderProduct op on oi.orderNum = op.orderId and oi.orderType=3";
            // orderInfoService.findById();

            String hql = "from OrderProduct obj ";
            hql = HqlUtil.addCondition(hql, "orderInfo.orderNum", "OR201705151223000003");
            Iterator<OrderProduct> iterator = orderProductService.list(hql).iterator();
            List<OrderProduct> list = new ArrayList<OrderProduct>();
            while (iterator.hasNext()) {
                list.add(iterator.next());
            }

            //System.out.println(JSONObject.toJSONString(list));


        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /*@Test
    public void test2() {
        try {
            String hql = "from TenantProduct obj ";
            hql = HqlUtil.addCondition(hql, "", "");
            Iterator<TenantProduct> iterator = tenantProductService.list(hql).iterator();
            List<TenantProduct> list = new ArrayList<TenantProduct>();
            while (iterator.hasNext()) {
                list.add(iterator.next());
            }

            System.out.println(JSONObject.toJSONString(list));


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }*/
}
