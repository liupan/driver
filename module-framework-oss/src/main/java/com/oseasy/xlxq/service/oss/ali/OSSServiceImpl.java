package com.oseasy.xlxq.service.oss.ali;

import com.google.gson.Gson;
import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.service.config.SystemConfig;
import com.oseasy.xlxq.service.core.utils.DateUtils;
import com.oseasy.xlxq.service.core.utils.UUIDGenerator;
import com.oseasy.xlxq.service.oss.OSSService;
import com.oseasy.xlxq.service.oss.SizePutRet;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Tandy on 2016/7/14.
 * 阿里云OSS 云存储方案实现
 * repository ==> oss bucket
 *
 */
@Service
public class OSSServiceImpl implements OSSService {

    public static final Logger logger = LoggerFactory.getLogger(OSSServiceImpl.class);
    //构造一个带指定Zone对象的配置类,这里是华东地区
    public static Configuration cfg = new Configuration(Zone.zone0());
    public static UploadManager uploadManager = new UploadManager(cfg);

    @Autowired
    private RedisCacheService redisCacheService;


    @Override
    public boolean uploadFileStream(InputStream inputStream, long fileLength, String orignalFileName, String repository, String fileKey) {
        if (logger.isDebugEnabled()){
            logger.debug("开始上传qiniu文件{}({})",orignalFileName,fileKey);
        }
        long starttime = System.currentTimeMillis();
        try {
            //获取上传凭证
            String upToken = redisCacheService.get("qiniu:upload:token");
            Response response = uploadManager.put(inputStream, fileKey, upToken,null, null);
            //解析上传成功的结果
            SizePutRet putRet = new Gson().fromJson(response.bodyString(), SizePutRet.class);
            System.out.println(putRet.key);
            System.out.println(putRet.hash);
            System.out.println(putRet.bucket);
            System.out.println(putRet.fsize);
        } catch (Exception e) {
            if (logger.isDebugEnabled()){
                logger.debug("上传qiniu文件{}({})失败，花费时间{} ms，原因{}",orignalFileName,fileKey,(System.currentTimeMillis()-starttime),e);
            }
            logger.error("上传qiniu文件失败",e);
            return false;
        }
        long endtime = System.currentTimeMillis();
        if (logger.isDebugEnabled()){
                logger.debug("上传qiniu文件{}({})成功 花费时间 {} ms",orignalFileName,fileKey,(endtime-starttime));
         }
        return true;
    }


    public String uploadFile(String tenantId,String folder, MultipartFile file) throws IOException {
        String name = file.getOriginalFilename();//文件名
        if(StringUtils.isNotBlank(name)){
            String type = name.substring(name.lastIndexOf("."),name.length());
            String ymd = DateUtils.formatDate(new Date(),"yyyyMMdd");
            String fileKey = "tenant_res/"+tenantId+"/"+folder+"/"+ymd+"/"+ UUIDGenerator.uuid()+type;
            long size = file.getSize();
            boolean flag = this.uploadFileStream(file.getInputStream(),size,name, SystemConfig
                .getProperty("global.oss.aliyun.bucket"),fileKey);
            if(flag){
                return fileKey;
            }else{
                throw new RuntimeException("上传文件失败");
            }
        }else{
            return null;
        }
    }
}
