package com.oseasy.xlxq.service.oss;

import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.qiniu.common.Zone;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class QiniuTokenSchemaTask {
    private static final Logger logger = LoggerFactory.getLogger(QiniuTokenSchemaTask.class);
    private static ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    private static final String ak = "BFw31p6QZHAD06cv8GxBYMbJPL0ci_DjSJ8fc4DO";
    private static final String sk = "OtmFAf7NiwGcx2N8W0N_CuJbgORizndQiahoxUGv";
    private static final String bucket = "open-sources";

    @Autowired
    private RedisCacheService redisCacheService;

    // 刷新wxjsticket到redis，redie的key为wxjsticket
    private class RefreshJsTicket implements Runnable{

        @Override
        public void run() {
            int tryTimes = 0;
            do {
                tryTimes++;
                try {
                    Auth auth = Auth.create(ak, sk);
                    StringMap putPolicy = new StringMap();
                    putPolicy.put("returnBody", "{\"key\":\"$(key)\",\"hash\":\"$(etag)\",\"bucket\":\"$(bucket)\",\"fsize\":$(fsize)}");
                    long expireSeconds = 7200;
                    String upToken = auth.uploadToken(bucket, null, expireSeconds, putPolicy);
                    redisCacheService.set("qiniu:upload:token", upToken, 7200);
                    System.out.println(upToken);
                    tryTimes = 6;
                } catch (Exception e) {
                    e.printStackTrace();
                    tryTimes++;
                    if (tryTimes == 5) {
                        logger.error("刷新token失败");
                    }
                }
            } while (tryTimes <= 5);
        }
    };

    @PostConstruct
    public void init(){
        scheduledExecutorService.scheduleAtFixedRate(new RefreshJsTicket(), 1, 7000, TimeUnit.SECONDS);
    }
}
