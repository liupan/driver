1. app-backend 系统后台运行的程序，处理一些特殊的任务，比如处理消息队列的任务
2. app-console 给商户使用的公众平台的接口服务
3. app-site 给公司管理员使用的后台管理的接口服务
4. app-wx 给小程序提供接口服务
5. module-framework-cache 数据缓存模块，基于redis
6. module-framework-config 系统的配置文件模块
7. module-framework-core 核心模块
8. module-framework-monitor 监控模块
9. module-framework-mq 消息队列模块
10. module-framework-oss 文件服务模块
11. module-framework-sms 短信邮件模块
12. module-framework-web web，http协议相关配置模块
13. module-service 分布式服务接口实现模块
14. module-service-api 分布式服务接口抽象模块