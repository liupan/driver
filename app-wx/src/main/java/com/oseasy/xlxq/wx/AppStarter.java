package com.oseasy.xlxq.wx;

import com.oseasy.xlxq.service.cache.manager.RedisCacheService;
import com.oseasy.xlxq.wx.constant.Constant;
import com.oseasy.xlxq.wx.utils.CommonUtils;
import com.oseasy.xlxq.wx.utils.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by cannan on 2017/5/15.
 */
@Component
public class AppStarter {

    @Autowired
    private RedisCacheService redisCacheService;
    @PostConstruct
    public void init(){

        String wxtoken = null;
        String token = redisCacheService.get("wxtoken");

        wxtoken = TokenUtil.getValidTokenFromRedis(token);
        if(CommonUtils.notEmpty(wxtoken)){
            redisCacheService.set("wxtoken",wxtoken,7100);
        }else {
            wxtoken = token;
        }


    }

}
