package com.oseasy.xlxq.wx.security;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by liups on 2016/6/21.
 */
public class AuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessHandler.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        super.onAuthenticationSuccess(request, response, authentication);
        if(logger.isDebugEnabled()){
            logger.debug("user login success:{}",authentication.getName());
        }
//        //将tocken保存到session
//        Object details = authentication.getDetails();
//        if(details instanceof String){
//            HttpSession session = request.getSession();
//            if(session != null) {
//                if(logger.isDebugEnabled()){
//                    logger.debug("登录成功,Token存入Session:{},{}", ConsoleConstants.SSO_TOKEN,details);
//                }
//                session.setAttribute(ConsoleConstants.SSO_TOKEN,details);
//            }
//        }
    }
}
