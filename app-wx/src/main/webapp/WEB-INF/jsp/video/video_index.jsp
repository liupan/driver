<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/5
  Time: 10:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>乡丽乡亲</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
</head>

<body ontouchstart class="bg_fafafa">
<!--主体部分 开始-->
<div class="content">
    <div class="swiper-container" data-space-between='10' data-pagination='.swiper-pagination' data-autoplay="1000">
        <div class="swiper-wrapper">
            <div class="swiper-slide"><img src="/resources/img/swiper-1.jpg" alt=""></div>
            <div class="swiper-slide"><img src="/resources/img/swiper-2.jpg" alt=""></div>
            <div class="swiper-slide"><img src="/resources/img/swiper-3.jpg" alt=""></div>
            <div class="swiper-slide"><img src="/resources/img/swiper-3.jpg" alt=""></div>
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination" id="on"></div>
    </div>
    <!--banner 结束-->
    <!--分类-->
    <div class="weui-grids whitebg" id="icon_btn">

    </div>
    <!--分类结束-->
    <div class="hr"></div>
    <!--最热-->
    <div class="main">
        <div class="whitebg weui-grids">
            <div class="title_hot">
                最热
            </div>
            <ul class="sp_list" id="hotroomlist">


            </ul>

        </div>
        <!--门票-->

    </div>

</div>
<!--主体部分结束-->
<!--底部固定导航 开始-->
<%@include file="/inc/footdown.jsp" %>
</body>
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>
<script src="/resources/js/plyr.js"></script>

<script>


    $(function () {
        getvideoChannel();
        getvideoRoomList();
    });



    //获取频道信息
    function getvideoChannel() {
        $.get("${ctx}/video/channellist",
            function(data) {
                var html = "";
                for(var i=0;i<data.data.result.length;i++){

                    html += '<a href="${ctx}/video/tovideoProject?channelid='+data.data.result[i].id+'" class="weui-grid js_grid" data-id="button">';
                    html += '<div class="weui-grid__icon">';
                    html += ' <img src="'+data.data.result[i].channelPic+'" alt="'+data.data.result[i].channelName+'">';
                    html += '</div>';
                    html += '<p class="weui-grid__label">'+data.data.result[i].channelName+'</p>';
                    html += '</a>';

                }
                html += '<a href="${cxt}/video/tomorechannel" class="weui-grid js_grid" data-id="button">';
                html += '<div class="weui-grid__icon">';
                html += '<img src="/resources/img/wechart1.png" alt="更多">'
                html += '</div>';
                html += ' <p class="weui-grid__label">更多</p>';
                html += ' </a>';

                $("#icon_btn").html(html);



            }
        );
    }


    //获取推荐直播间列表
    function getvideoRoomList() {
        $.get("${ctx}/video/recommendroomlist",
            function(data) {
                var html = "";
                for(var i=0;i<data.data.length;i++){
                    html += '<li>';
                    html += '<a href="${cxt}/video/videoroomDetails?roomid='+data.data[i].id+'" title="">';
                    html += '<div class="pic_div"><img src="'+data.data[i].livecover+'"></div>';
                    html += '<div class="tit_div_line">'+data.data[i].roomname;
                    html += '<span >';
                    html += '<i class="lookicon"></i>';
                    html += '</span>';
                    html += '</div>';
                    html += '</a>';
                    html += '</li>';

                }

                $("#hotroomlist").html(html);



            }
        );
    }

    $(".swiper-container").swiper({
        loop: true,
        autoplay: 3000
    });
</script>

</html>
