<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/9
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>查看物流</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />

</head>

<body ontouchstart>
<div class="weui-cells m-t-0">
    <div class="weui-cell log">
        <div class="weui-cell__bd weui-cell_primary">
            <p class="recipient">
                <span id="ordernum"></span>
                <span class="fr" id="ordertime">

						</span>

            </p>
        </div>
        <div class="weui-cell__ft"> </div>
    </div>
</div>
<div id="checkstate" class="main"></div>
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/template.js"></script>



    <div class="package-status" id="logisticsinfo">

    </div>





<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>

<script>

    $(function () {
        getorderinfo();
        getlogisticsinfo();
    })


    function getlogisticsinfo() {
        $.get("${ctx}/order/getlogisticinfo",
            function(data) {
            var html = "";
                if(data.data.Success){
                    html += ' <ul class="loglist" >';
//                    html += ' <li class="specialColor">';
//                    html += '<div class="status">';
//                    html += '<span>'+data.data.Traces[0].AcceptStation+'</span><br />';
//                    html += '<span class="logdate">'+data.data.Traces[0].AcceptTime+'</span>';
//                    html += ' </div>';
//                    html += ' </li>';
                    for (var i=0;i<data.data.Traces.length;i++){
                        if (i == data.data.Traces.length - 1){
                            html += ' <li class="specialColor">';
                        }else {
                            html += ' <li >';
                        }

                        html += '<div class="status">';
                        html += '<span>'+data.data.Traces[i].AcceptStation+'</span><br />';
                        html += '<span class="logdate">'+data.data.Traces[i].AcceptTime+'</span>';
                        html += ' </div>';
                        html += ' </li>';


                    }
                    html += '</ul>';

                }else{
                    html = '<h3>'+data.data.Reason+'</h3>';
                }
                $("#logisticsinfo").html(html);

            }
        );
    }

    //获取订单信息
    function  getorderinfo() {
        $.get("${ctx}/order/getuserorderinfo",
            function(data) {
            var html = "";
                if(data.success){
                    html = '订单号：<em>'+data.data.orderNum+'</em>';
                    $("#ordernum").html(html);
                    var time1 = new Date(data.data.createTime).Format("yyyy-MM-dd hh:mm:ss");
                    html = '时间：<em class="orderdate">'+time1+'</em>';
                    $("#ordertime").html(html);
                }

            }
        );
    }


    // 对Date的扩展，将 Date 转化为指定格式的String
    // 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
    // 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
    // 例子：
    // (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
    // (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {        "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "h+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));    for (var k in o)    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));    return fmt;}
</script>

</body>

</html>
