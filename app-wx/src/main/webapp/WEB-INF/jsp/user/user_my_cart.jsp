<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/13
  Time: 11:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>购物车</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
</head>

<body class="bg_fafafa">
<div class="main content">
    <form>
        <div class="weui-cells weui-cell_access writeaddress m-t-0">
            <a href="../order/product_writeAddress.html" class="weui-cell">
                <div class="weui-cell__bd weui-cell_primary">
                    <input id="addressid" value="" type="hidden"></input>
                    <p class="recipient">
                        <span class="recipientmen">收件人：杨梦妮</span>
                        <span class="fr">18267148003</span>

                        <br>
                        <span class="hidden"><i class="sstfont sst-dizhi fl"></i> <i class="shouhuo">湖北省武汉市洪山区关山大道曙光村光谷创意大厦C座17楼</i></span></p>
                </div>
                <div class="weui-cell__ft"> </div>
            </a>
        </div>
    </form>

    <div id="allcart">

    </div>
    <div class="cart_list">

        <div class="weui-panel weui-panel_access">
            <p class="title_flower border-bottom"><span class="allchecked cart-checkbox1" state="0" ></span><i></i>果农1</p>
            <div class="weui-panel__bd">


                <div class="weui-media-box weui-media-box_appmsg oneGood" goodid="1">
                    <div class="weui-media-box__hd" style="padding-left:40px;">
                        <div class="check-wrapper" goodid="1">
                            <i class="cart-checkbox" state="0">
                                <input type="checkbox" autocomplete="off" name="sel_cartgoods[]" value="274"  style="display:none;">
                            </i>
                        </div>
                        <img class="weui-media-box__thumb" src="../img/58f1e633c8ef3_64_64.jpg" alt="">
                    </div>
                    <div class="weui-media-box__bd good">
                        <span class="delete oneGoodDelBtn" goodId="1"></span>
                        <div class="tit_div">无敌火龙果无敌火龙果无敌火龙 果无敌火龙果无敌火龙果无</div>
                        <div class="m-b-10">
                            <em class="shipping">已售44</em>

                        </div>
                        <div class="price_div">
									<span class="product-price1">¥<span class="big-price oneGoodPrice">98</span>
									<span class="small-price">.00</span>
									<em class="originalprice">￥180</em></span>
                            <span class="product-xiaoliang">快递：免运费</span>
                        </div>

                        <div class="weui-cell wellbox">
                            <div class="weui-cell__bd weui-cell_primary">
                                <div class="shopping_count">
                                    <span class="fr"><em class="youhui">(已优惠33.0元)</em></span>
                                </div>
                            </div>
                            <div style="font-size: 0px;" class="weui-cell__ft">
                                <span class="number-selector number-selector-sub needsclick" goodid="1">-</span>
                                <input id="qty_num" pattern="[0-9]*" class="number-input oneGoodNum" style="width: 50px;" value="1" data-min="1" data-step="1" goodid="1">
                                <span class="number-selector number-selector-plus needsclick" goodid="1">+</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>

    <div class="cart_list">
        <div class="weui-panel weui-panel_access">
            <p class="title_flower border-bottom"><span class="allchecked cart-checkbox1" state="0"></span><i></i>果农1</p>

            <div class="weui-panel__bd">
                <div class="weui-media-box weui-media-box_appmsg oneGood" goodid="3">
                    <div class="weui-media-box__hd" style="padding-left:40px;">
                        <div class="check-wrapper" goodid="3">
                            <i class="cart-checkbox" state="0">
                                <input type="checkbox" autocomplete="off" name="sel_cartgoods[]" value="274"  style="display:none;">
                            </i>
                        </div>
                        <img class="weui-media-box__thumb" src="../img/58f1e633c8ef3_64_64.jpg" alt="">
                    </div>
                    <div class="weui-media-box__bd good">
                        <span class="delete oneGoodDelBtn" goodId="3"></span>
                        <div class="tit_div">无敌火龙果无敌火龙果无敌火龙 果无敌火龙果无敌火龙果无</div>
                        <div class="m-b-10">
                            <em class="shipping">已售44</em>

                        </div>
                        <div class="price_div">
									<span class="product-price1">¥<span class="big-price oneGoodPrice">10</span>
									<span class="small-price">.00</span>
									<em class="originalprice">￥180</em></span>
                            <span class="product-xiaoliang">快递：免运费</span>
                        </div>

                        <div class="weui-cell wellbox">
                            <div class="weui-cell__bd weui-cell_primary">
                                <div class="shopping_count">
                                    <span class="fr"><em class="youhui">(已优惠33.0元)</em></span>
                                </div>
                            </div>
                            <div style="font-size: 0px;" class="weui-cell__ft">
                                <span class="number-selector number-selector-sub needsclick" goodId="3">-</span>
                                <input id="qty_num" pattern="[0-9]*" class="number-input oneGoodNum" style="width: 50px;" value="1" data-min="1" data-step="1" goodId="3">
                                <span class="number-selector number-selector-plus needsclick" goodId="3">+</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="weui-panel__bd">
                <div class="weui-media-box weui-media-box_appmsg oneGood" goodid="4">
                    <div class="weui-media-box__hd" style="padding-left:40px;">
                        <div class="check-wrapper" goodid="4">
                            <i class="cart-checkbox" state="0">
                                <input type="checkbox" autocomplete="off" name="sel_cartgoods[]" value="274"  style="display:none;">
                            </i>
                        </div>
                        <img class="weui-media-box__thumb" src="../img/58f1e633c8ef3_64_64.jpg" alt="">
                    </div>
                    <div class="weui-media-box__bd good">
                        <span class="delete oneGoodDelBtn" goodid="4"></span>
                        <div class="tit_div">无敌火龙果无敌火龙果无敌火龙 果无敌火龙果无敌火龙果无</div>
                        <div class="m-b-10">
                            <em class="shipping">已售44</em>

                        </div>
                        <div class="price_div">
									<span class="product-price1">¥<span class="big-price oneGoodPrice">98</span>
									<span class="small-price">.00</span>
									<em class="originalprice">￥180</em></span>
                            <span class="product-xiaoliang">快递：免运费</span>
                        </div>

                        <div class="weui-cell wellbox">
                            <div class="weui-cell__bd weui-cell_primary">
                                <div class="shopping_count">
                                    <span class="fr"><em class="youhui">(已优惠33.0元)</em></span>
                                </div>
                            </div>
                            <div style="font-size: 0px;" class="weui-cell__ft">
                                <span class="number-selector number-selector-sub needsclick" goodid="4">-</span>
                                <input id="qty_num" pattern="[0-9]*" class="number-input oneGoodNum" style="width: 50px;" value="1" data-min="1" data-step="1" goodid="4">
                                <span class="number-selector number-selector-plus needsclick" goodid="4">+</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!--底部固定导航 开始-->
<div class="payment-total-bar" id="payment">
    <div class="shp-chk box-flex-c choose_all">
        <span class="cart-checkbox" id="checkIcon-1" state="0"></span>
        <!--<span class="cart-checkbox-text">全选</span>-->
        全选
    </div>
    <div class="shp-cart-info shp-cart-info-new  box-flex-c">
        <p id="shpCartTotal" data-fsizeinit="14" class="shp-cart-total">
            合计:<span class="pieces" id="checkedNum">0</span>件<span class="combined_total">￥<em id="cart_realPrice">0</em><i>.60</i></span>
        </p>

    </div>
    <a class="btn-right-block btn-right-block-new  box-flex-c" id="submit">
        <p class="submit">提交订单</p>
        <p class="share">共优惠<em>33.00</em>元</p>
    </a>
</div>

<script src="/resources/js/jquery-2.1.4.js"></script>
<script src="/resources/js/jquery-weui.js"></script>
<script src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js"></script>

<script type="text/javascript">
//    $(document).ready(function() {
//        //to_buy(); //去结算
//        function to_buy() {
//            $('#submit').click(function() {
//                var theurl = "/shop/index/to_creat_order.html";
//                var goodsinfo = data = JSON.stringify(choosed_goods_info);
//                $.post(theurl, {
//                    goods_info: goodsinfo,
//                    isAjax: 1
//                }, function(msg) {
//                    //console.log(msg);return false;
//                    if(msg.state) {
//                        window.location.href = theurl;
//                    }
//                })
//            })
//        }
//
//        //选中的商品id，选中商品对应的价格，选中商品对应的数量
//        var cart_goods_info = new Array(); //存放购物车中商品的信息
//        var choosed_goods_info = new Array(); //用户选中的商品信息
//
//        function init_cart_goods_info() { //购物车中的商品信息初始化,获取需要的字段
//            var elem_oneGood = $('.oneGood'),
//                elem_oneGoodPrice = $('.oneGoodPrice'),
//                elem_oneGoodNum = $('.oneGoodNum');
//            var cart_goods_num = elem_oneGood.length; //购物车中的所有商品数
//            var good_id, good_num, good_price;
//            for(var i = 0; i < cart_goods_num; i++) {
//                good_id = elem_oneGood.eq(i).attr('goodId');
//                good_num = elem_oneGoodNum.eq(i).val();
//                good_price = elem_oneGoodPrice.eq(i).text();
//                cart_goods_info[i] = {
//                    'good_id': good_id,
//                    'good_num': good_num,
//                    'good_price': good_price
//                };
//            }
//            //choosed_goods_info=JSON.parse( JSON.stringify(cart_goods_info) );//对象直接复制为引用,此处拷贝
//        }
//
//        //calculate_count();
//        function calculate_count() { //计算总价格
//            var count = 0;
//            var all_num = 0;
//            for(var i = 0; i < choosed_goods_info.length; i++) {
//                count += choosed_goods_info[i]['good_num'] * choosed_goods_info[i]['good_price'];
//                all_num += parseInt(choosed_goods_info[i]['good_num']);
//            }
//            $('#cart_realPrice').text(count);
//
//            $('#checkedNum').text(all_num); //选中的总数
//            return count;
//        }
//
//        function change_buy_num(good_id, buy_num) { //改变商品购买数量
//            for(var i = 0; i < cart_goods_info.length; i++) {
//                if(cart_goods_info[i]['good_id'] == good_id) {
//                    cart_goods_info[i]['good_num'] = buy_num;
//                    break;
//                }
//            }
//            for(var i = 0; i < choosed_goods_info.length; i++) {
//                if(choosed_goods_info[i]['good_id'] == good_id) {
//                    choosed_goods_info[i]['good_num'] = buy_num;
//                    break;
//                }
//            }
//        }
//
//        function change_choosed_goods(good_id, choosetype) { //选择或取消选择购物车中的商品,choosetype为true选择，false取消选择
//            if(choosetype) {
//                for(var i = 0; i < cart_goods_info.length; i++) {
//                    if(good_id == cart_goods_info[i]['good_id']) {
//                        choosed_goods_info.push(cart_goods_info[i]);
//                        break;
//                    }
//                }
//            } else {
//                choosed_goods_info = deldata(choosed_goods_info, good_id);
//            }
//        }
//
//        function change_choose_callback(good_id, choosetype) { //选择或取消选择回调
//            change_choosed_goods(good_id, choosetype);
//            calculate_count();
//        }
//
//        function changenum_callback(good_id, buy_num) { //改变商品数量回调
//            change_buy_num(good_id, buy_num);
//            calculate_count();
//        }
//
//        function deldata(dataobj, good_id) { //删除存储商品信息数组中某一商品信息
//            var choosed_temp = new Array();
//            for(var i = 0; i < dataobj.length; i++) {
//                if(good_id != dataobj[i]['good_id']) {
//                    choosed_temp.push(dataobj[i]);
//                }
//            }
//            return choosed_temp;
//        }
//
//        function del_cartgood_callback(good_id) { //删除购物车中对应商品的信息数据
//            cart_goods_info = deldata(cart_goods_info, good_id);
//            choosed_goods_info = deldata(choosed_goods_info, good_id);
//            calculate_count();
//            var good_domobj = $('.oneGood');
//            for(var i = 0; i < good_domobj.length; i++) {
//                if(good_domobj.eq(i).attr('goodId') == good_id) {
//                    good_domobj.eq(i).remove();
//                }
//            }
//        }
//
//        del_cart_good(del_cartgood_callback);
//
//        function del_cart_good(callback) { //删除购物车中商品
//            $('.oneGoodDelBtn').each(function(i) {
//                this.onclick = function() {
//                    var goodid = $(this).attr('goodId');
//                    $.confirm("", "您确定要删除吗?", function() {
//                        $.toast("删除成功");
//                    }, function() {
//                        //取消操作
//                    });
//                    //var url = "/shop/index/cart_del_item.html";
//                    //$.post(url, { good_id: goodid }, function(msg) {
//                    //if(msg.status) {
//
//                    callback(goodid);
//
//                    //}
//                    //})
//                }
//            });
//        }
//
//        var one_choose_elem = $('.check-wrapper');
//        radio_checkbox(one_choose_elem, change_choose_callback); //选择商品按钮点击
//        function radio_checkbox(elem, callback, callback2) { //单选按钮特效
//            elem.each(function(i) {
//                this.onclick = function() {
//                    var cur_checkbox = $(this).find('.cart-checkbox'); //$('.cart-checkbox').eq(i);
//                    var checkbox_state = cur_checkbox.attr('state'); // alert(checkbox_state);
//                    var checkstate = 0; //选择的状态
//                    if(checkbox_state == 1) {
//                        cur_checkbox.removeClass('checked');
//                        cur_checkbox.attr('state', 0);
//                        $(this).parents(".cart_list").find(".allchecked").removeClass('checked');
//                        $(this).parents(".cart_list").find(".allchecked").attr('state', 0);
//                        $("#checkIcon-1").removeClass('checked');
//                        $("#checkIcon-1").attr('state', 0);
//                        checkstate = 0;
//
//                    } else {
//                        cur_checkbox.addClass('checked');
//                        cur_checkbox.attr('state', 1);
//                        checkstate = 1;
//                        $(this).parents(".cart_list").find(".allchecked").addClass('checked');
//                        $(this).parents(".cart_list").find(".allchecked").attr('state', 1);
//                        init_cart_goods_info();
//
//                    }
//
//                    if(callback) { //选择商品回调
//                        var good_id = $(this).attr('goodId');
//                        callback(good_id, checkstate);
//
//                    }
//
//                    if(callback2) { //全选回调
//                        callback2(checkstate);
//
//                    }
//                }
//            });
//        }
//
//        var one_choose_store = $('.allchecked');
//        radio_checkbox_good(one_choose_store, change_choose_callback); //选择商品按钮点击
//        function radio_checkbox_good(elem, callback2) { //单选按钮特效
//            elem.each(function(i) {
//                this.onclick = function() {
//                    var storegood_checkbox = $(this).parents(".cart_list").find(".cart-checkbox"); //$('.cart-checkbox').eq(i);
//                    var checkbox_state = storegood_checkbox.attr('state'); // alert(checkbox_state);
//                    var checkstate = 0; //选择的状态
//                    if(checkbox_state == 1) {
//                        $(this).removeClass('checked');
//                        $(this).attr('state', 0);
//                        storegood_checkbox.removeClass('checked');
//                        storegood_checkbox.attr('state', 0);
//                        checkstate = 0;
//                        choosed_goods_info = new Array();
//
//                    } else {
//                        $(this).addClass('checked');
//                        $(this).attr('state', 1);
//                        storegood_checkbox.addClass('checked');
//                        storegood_checkbox.attr('state', 1);
//                        checkstate = 1;
//                        choosed_goods_info = cart_goods_info;
//                        init_cart_goods_info();
//
//
//                    }
//                    //全选回调
//                    if(callback2) {
//                        callback2(checkstate);
//                    }
//                }
//            });
//        }
//
//
//        /*全选*/
//        function all_choose_callback(choosetype){
//            var goods_choose_state=$('.check-wrapper').find('.cart-checkbox');
//            var store_choose = $(".allchecked");
//            if(choosetype){
//                goods_choose_state.addClass('checked');
//                goods_choose_state.attr('state',1);
//                store_choose.addClass('checked');
//                store_choose.attr('state',1);
//                choosed_goods_info=cart_goods_info;
//            }else{
//                goods_choose_state.removeClass('checked');
//                goods_choose_state.attr('state',0);
//                store_choose.removeClass('checked');
//                store_choose.attr('state',0);
//                choosed_goods_info=new Array();
//            }
//            calculate_count();
//        }
//
//        var all_choose_elem=$('.choose_all');
//        radio_checkbox(all_choose_elem,false,all_choose_callback);
//
//
//
//
//        $('.number-selector-plus').click(function() {
//            upDownOperation($(this), changenum_callback);
//        });
//        $('.number-selector-sub').click(function() {
//            upDownOperation($(this), changenum_callback);
//        });
//
//        function upDownOperation(element, callback) {
//            var _input = element.parent().find('input'),
//                _value = _input.val(),
//                _step = _input.attr('data-step') || 1;
//            //检测当前操作的元素是否有disabled，有则去除
//            element.hasClass('disabled') && element.removeClass('disabled');
//            //检测当前操作的元素是否是操作的添加按钮（.input-num-up）'是' 则为加操作，'否' 则为减操作
//            if(element.hasClass('number-selector-plus')) {
//                var _new_value = parseInt(parseFloat(_value) + parseFloat(_step)),
//                    _max = _input.attr('data-max') || false,
//                    _down = element.parent().find('.number-selector-sub');
//
//                //若执行'加'操作且'减'按钮存在class='disabled'的话，则移除'减'操作按钮的class 'disabled'
//                _down.hasClass('disabled') && _down.removeClass('disabled');
//                if(_max && _new_value >= _max) {
//                    _new_value = _max;
//                    element.addClass('disabled');
//                }
//            } else {
//                var _new_value = parseInt(parseFloat(_value) - parseFloat(_step)),
//                    _min = _input.attr('data-min') || false,
//                    _up = element.parent().find('.number-selector-plus');
//                //若执行'减'操作且'加'按钮存在class='disabled'的话，则移除'加'操作按钮的class 'disabled'
//                _up.hasClass('disabled') && _up.removeClass('disabled');
//                if(_min && _new_value <= _min) {
//                    _new_value = _min;
//                    element.addClass('disabled');
//                }
//            }
//            _input.val(_new_value);
//
//            if(callback) {
//                var goods_id = element.attr('goodId');
//                callback(goods_id, _new_value);
//            }
//        };
//    });
    var largePrice = "";
    var smallPrice = "";


    $(function () {
        getaddressdetail();
        getcartinfo();
    })



    //获取地址详情
    function getaddressdetail() {

        $.get("${ctx}/useradrs/getinfobyid",
            function(data) {
                $("#addressid").val(data.data.id);
                var html = ' <span class="recipientmen" >收件人：'+data.data.consignee+'</span>';
                html += '<span class="fr">'+data.data.receivingTel+'</span>';
                html += '<br>';
                html += '<span class="hidden"><i class="sstfont sst-dizhi fl"></i> <i class="shouhuo" >'+data.data.receivingAddress+'</i></span>';

                $("#addressinfo").html(html);

            }
        );
    }

    //获取购物车信息
    function getcartinfo() {
        $.get("${cxt}/usermy/getcartproinfo",
            function (data) {
                var html = "";
                for(var i =0;i<data.data.length;i++){
                    returnFloat(data.data[i].tenantProduct.favorablePrice);
                    var youhui = data.data[i].tenantProduct.productUnit - data.data[i].tenantProduct.favorablePrice;
                    html += '<div class="cart_list" id="'+data.data[i].tenantMerchant.id+'">';
                    html += '<div class="weui-panel weui-panel_access">';
                    html += ' <p class="title_flower border-bottom"><span class="allchecked cart-checkbox1" state="0" ></span><i></i>'+data.data[i].tenantMerchant.merchantName+'</p>';
                    html += '<div class="weui-panel__bd">';
                    html += '<div class="weui-media-box weui-media-box_appmsg oneGood" goodid="'+data.data[i].tenantProduct.id+'">';
                    html += '<div class="weui-media-box__hd" style="padding-left:40px;">';
                    html += '<div class="check-wrapper" goodid="'+data.data[i].tenantProduct.id+'">';
                    html += ' <i class="cart-checkbox" state="0">';
                    html += '<input type="checkbox" autocomplete="off" name="sel_cartgoods[]" value="274"  style="display:none;">';
                    html += '</i>';
                    html += '</div>';
                    html += '<img class="weui-media-box__thumb" src="'+data.data[i].tenantProduct.titleImgUrl+'" alt="">';
                    html += '</div>';
                    html += ' <div class="weui-media-box__bd good">';
                    html += '<span class="delete oneGoodDelBtn" goodid="'+data.data[i].tenantProduct.id+'"></span>';
                    html += '<div class="tit_div">'+data.data[i].tenantProduct.productName+'</div>';
                    html += '<div class="m-b-10">';
                    html += '<em class="shipping">已售'+data.data[i].tenantProduct.salesVolume+'</em>';
                    html += '</div>';
                    html += '<div class="price_div">';
                    html += '<span class="product-price1">¥<span class="big-price oneGoodPrice">'+largePrice+'</span>';
                    html += '<span class="small-price">'+smallPrice+'</span>';
                    html += '<em class="originalprice">￥'+data.data[i].tenantProduct.productUnit+'</em></span>';
                    if(data.data[i].tenantProduct.distributionType == "包邮"){
                        html += '<span class="product-xiaoliang">快递：免运费</span>';
                    }else{
                        html += '<span class="product-xiaoliang">'+data.data[i].tenantProduct.distributionType+'<em class="freight">'+data.data[i].tenantProduct.freight+'</em></span>';
                    }
                    html += ' </div>';
                    html += '<div class="weui-cell wellbox">';
                    html += '<div class="weui-cell__bd weui-cell_primary">';
                    html += '<div class="shopping_count">';
                    html += '<span class="fr"><em class="youhui">(已优惠'+youhui+'元)</em></span>';
                    html += ' </div></div>';
                    html += '<div style="font-size: 0px;" class="weui-cell__ft">';
                    html += '<span class="number-selector number-selector-sub needsclick" goodid="'+data.data[i].tenantProduct.id+'">-</span>';
                    html += '<input id="qty_num" pattern="[0-9]*" class="number-input oneGoodNum" style="width: 50px;" value="1" data-min="1" data-step="1" goodid="'+data.data[i].tenantProduct.id+'">';
                    html += '<span class="number-selector number-selector-plus needsclick" goodid="'+data.data[i].tenantProduct.id+'">+</span>';
                    html += ' </div></div></div></div></div>';

                }

                $("#allcart").html(html);
//
//
//
//
//
//
//
//
//
//
//
//
//
//                    <div class="weui-panel__bd">
//                    <div class="weui-media-box weui-media-box_appmsg oneGood" goodid="2">
//                    <div class="weui-media-box__hd" style="padding-left:40px;">
//                    <div class="check-wrapper" goodid="2">
//                    <i class="cart-checkbox" state="0">
//                    <input type="checkbox" autocomplete="off" name="sel_cartgoods[]" value="274"  style="display:none;">
//                    </i>
//                    </div>
//                    <img class="weui-media-box__thumb" src="../img/58f1e633c8ef3_64_64.jpg" alt="">
//                    </div>
//                    <div class="weui-media-box__bd good">
//                    <span class="delete oneGoodDelBtn" goodid="2"></span>
//                    <div class="tit_div">无敌火龙果无敌火龙果无敌火龙 果无敌火龙果无敌火龙果无</div>
//                <div class="m-b-10">
//                    <em class="shipping">已售44</em>
//
//                    </div>
//                    <div class="price_div">
//                    <span class="product-price1">¥<span class="big-price oneGoodPrice">100</span>
//                    <span class="small-price">.00</span>
//                    <em class="originalprice">￥180</em></span>
//                    <span class="product-xiaoliang">快递：免运费</span>
//                </div>
//
//                <div class="weui-cell wellbox">
//                    <div class="weui-cell__bd weui-cell_primary">
//                    <div class="shopping_count">
//                    <span class="fr"><em class="youhui">(已优惠33.0元)</em></span>
//                </div>
//                </div>
//                <div style="font-size: 0px;" class="weui-cell__ft">
//                    <span class="number-selector number-selector-sub needsclick" goodid="2">-</span>
//                    <input id="qty_num" pattern="[0-9]*" class="number-input oneGoodNum" style="width: 50px;" value="1" data-min="1" data-step="1" goodid="2">
//                    <span class="number-selector number-selector-plus needsclick" goodid="2">+</span>
//                    </div>
//                    </div>
//                    </div>
//
//                    </div>
//                    </div>
//                    </div>
//                    </div>
        })
    }


function returnFloat(value){
    var value=Math.round(parseFloat(value)*100)/100;
    var xsd=value.toString().split(".");
    if(xsd.length==1){
        value=value.toString()+".00";
        result = value .split(".");
        largePrice = result[0];
        smallPrice = '.'+ result[1];

    }
    if(xsd.length>1){
        if(xsd[1].length<2){
            value=value.toString()+"0";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }

    }

}

</script>

</body>

</html>
