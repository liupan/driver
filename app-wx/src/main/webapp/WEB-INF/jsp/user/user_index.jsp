<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/6/5
  Time: 19:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="applicable-device" content="mobile">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

    <!--苹果专用-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="乡丽乡亲">
    <title>我</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
</head>

<body class="bg_fafafa" ontouchstart>
<div class="content main">
    <div class="picheader">
        <div class="picture"><img id="headimg" src="/resources/img/pic.png" width="68" height="68" /></div>
        <h2><a href="${cxt}/user/edituserinfo" id="username">Deadeasy</a></h2>
    </div>
    <div class="weui-cells">
        <div class="weui-cell weui-cell_access">
            <div class="weui-cell__bd">我的订单</div>
            <div class="weui-cell__ft" style="font-size: 0">
                <span style="vertical-align:middle; font-size: 15px;"><a href="${cxt}/user/toorderlist?ordertype=">查看全部订单</a></span>

            </div>
        </div>
    </div>
    <div class="weui-flex icon_menu">
        <div class="weui-flex__item">
            <a href="${cxt}/user/toorderlist?ordertype=1">

                <div class="gridicon">
                    <span class="weui-badge" id="pendpay" style="display:none"></span>
                    <img src="/resources/img/ok.png" width="22" />
                </div>
                <p class="weui-grid__label">
                    待支付
                </p>
            </a>
        </div>
        <div class="weui-flex__item">
            <a href="${cxt}/user/toorderlist?ordertype=2">
                <div class="gridicon">
                    <span class="weui-badge" id="shippend" style="display:none"></span>
                    <img src="/resources/img/fahuo.png" width="22" />
                </div>
                <p class="weui-grid__label">
                    待发货
                </p>
            </a>
        </div>
        <div class="weui-flex__item">
            <a href="${cxt}/user/toorderlist?ordertype=3">
                <div class="gridicon">
                    <span class="weui-badge" id="waitrec" style="display:none"></span>
                    <img src="/resources/img/daishouhuo.png" width="22" />
                </div>
                <p class="weui-grid__label">
                    待收货
                </p>
            </a>
        </div>
        <div class="weui-flex__item">
            <a href="${cxt}/user/toorderlist?ordertype=7">
                <div class="gridicon">
                    <span class="weui-badge" id="pendevalu" style="display:none"></span>
                    <img src="/resources/img/bianji.png" width="18" />
                </div>
                <p class="weui-grid__label">
                    待评价
                </p>
            </a>
        </div>
    </div>
    <!--    订单列表-->
    <div class="weui-cells list">
        <a class="weui-cell weui-cell_access" href="${cxt}/user/tomycart">
            <div class="weui-cell__hd">
                <img src="/resources/img/cart.png" width="22" alt="">
                <span class="weui-badge">8</span>
            </div>
            <div class="weui-cell__bd">
                <p>购 &nbsp;物  &nbsp;车</p>
            </div>
            <div class="weui-cell__ft"></div>
        </a>
        <a class="weui-cell weui-cell_access" href="${cxt}/useradrs/toadreslist">
            <div class="weui-cell__hd">
                <img src="/resources/img/address.png" width="19" alt="" >

            </div>
            <div class="weui-cell__bd">
                <p>地址管理</p>
            </div>
            <div class="weui-cell__ft"></div>
        </a>
        <a class="weui-cell weui-cell_access" href="${cxt}/user/toorderlist?ordertype=20">
            <div class="weui-cell__hd"><img src="/resources/img/shouhuo.png" width="19" alt="" ></div>
            <div class="weui-cell__bd">
                <p>我的售后</p>
            </div>
            <div class="weui-cell__ft"></div>
        </a>


    </div>

    <div class="weui-cells list">
        <a class="weui-cell weui-cell_access" href="${cxt}/user/tomyfeedback">
            <div class="weui-cell__hd"><img src="/resources/img/yijianfk.png" width="16"  alt="" ></div>
            <div class="weui-cell__bd">
                <p>意见反馈</p>
            </div>
            <div class="weui-cell__ft"></div>
        </a>
    </div>
</div>

<!--底部固定导航 开始-->
<%@include file="/inc/footdown.jsp" %>
</body>

<script src="/resources/js/jquery-2.1.4.js"></script>
<script src="/resources/js/jquery-weui.js"></script>
<script src="/resources/js/fastclick.js"></script>
</body>
<script>
    $(function () {
        getOrderNuminfo();
        getuserinfo();
    })
    
    function getOrderNuminfo() {
        $.get("${ctx}/usermy/getallOrderinfo",
            function(data) {
                if(data.data.pendpaynum != "0"){
                    $("#pendpay").css("display","block");
                    $("#pendpay").html(data.data.pendpaynum);
                }else if(data.data.shippendnum != "0"){
                    $("#shippend").css("display","block");
                    $("#shippend").html(data.data.shippendnum);
                }else if(data.data.waitrecnum != "0"){
                    $("#waitrec").css("display","block");
                    $("#waitrec").html(data.data.waitrecnum);
                }else if(data.data.pendevalunum != "0"){
                    $("#pendevalu").css("display","block");
                    $("#pendevalu").html(data.data.pendevalunum);
                }

            }
        );
    }


    //获取用户信息
    function getuserinfo() {
        $.get("${ctx}/userinfo/getuserinfo",
            function(data) {
                $("#username").html(data.data.tenantUser.name);
                $("#headimg").attr('src',data.data.wxheadUrl);


            }
        );
    }
    
</script>
</html>
