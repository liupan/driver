<%--
  Created by IntelliJ IDEA.
  User: cannan
  Date: 2017/5/31
  Time: 18:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>订单确认</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
</head>

<body class="bg_fafafa" ontouchstart>
<h1 class="demo_title" id="tickettitle">九寨沟景区</h1>
<div class="content">
    <div class="main">
        <form method="post" id="formobjorder" action="prpduct_addresslist.html">
            <div class="weui-cells m-t-0">
                <div class="weui-cell">
                    <div class="weui-cell__hd">
                        <label class="weui-label">姓名</label>
                    </div>
                    <div class="weui-cell__bd weui-cell_primary">
                        <input class="weui-input" type="text" name="name" value="雪瑞" placeholder="请输入姓名" id="username">
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__hd">
                        <label class="weui-label">电话</label>
                    </div>
                    <div class="weui-cell__bd weui-cell_primary">
                        <input class="weui-input" type="number" name="phone" value="" pattern="[0-9]*" placeholder="请输入号码" id="userphone">
                    </div>
                </div>

                <div class="weui-cell">
                    <div class="weui-cell__bd weui-cell_primary">
                        <div>
                            <span>购买数量</span>

                        </div>
                    </div>

                    <div class="preferential">
                        <div style="font-size: 0px; overflow: hidden;" class="weui-cell__ft">
                            <a class="number-selector number-selector-sub needsclick">-</a>
                            <input id="qty_num" pattern="[0-9]*" class="number-input" style="width: 50px;" value="1" data-min="1" data-step="1">
                            <a class="number-selector number-selector-plus needsclick">+</a>
                        </div>
                        <div style="clear: both;"></div>
                        <div class="titaler">(已优惠<em class="youh">33.00</em>元)</div>
                    </div>
                </div>
                <div class="weui-cell">

                    <div class="weui-flex__item">

                    </div>
                    <div class="calculate fr orderbtn">
                        <span>共计<em class="pieces">2</em>间</span>
                        <!--	<span class="subtotal">小计:￥<em>75.</em>60</span>-->
                        <span class="subtotal">小计：<em class="totalpro">75</em><i>.60</i></span> </div>

                </div>

            </div>
        </form>
    </div>
</div>

<!--底部固定导航 开始-->
<div id="cart1" class="cart-concern-btm-fixed five-column four-column" style="display: table; z-index: 0;">
    <ul class="orderbtn">
        <li class="subtotalInfo">
            <a href="tel:13545350035" class="customer">
                <em class="cusicon"></em>
                <p>联系客户</p>
            </a>
            <div class="calculate fr">
                <span class="combined_total">合计：<em>75.</em><i>60</i></span>
            </div>
        </li>
        <li class="orderOk">
            <button class="weui-btn weui-btn_primary submitOrderBtn">提交订单</button>
        </li>
    </ul>

</div>
<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>

<script type="text/javascript" src="/resources/js/fastclick.js"></script>
<script type="text/javascript" src="/resources/js/app.js" ></script>

<script type="text/javascript">

    var largePrice = "";
    var smallPrice = "";
    var sellpriceall = "";
    var productUnit ="";
    function trim(str){
        return str.replace(/(^\s*)|(\s*$)/g, "");
    }
    $(function(){
        getuserinfo();
        getticketdetail();

        $('.submitOrderBtn').click(function() {
            var name = $("input[name='name']").val();
            var phone = $("input[name='phone']").val();

            if(trim(name) == '') {
                $.toast('请填写姓名');
                return false;
            }
            if(trim(phone) == '') {
                $.toast('请填写手机号');
                return false;
            }
            if(!(/^1[3|4|5|7|8]\d{9}$/.test(phone))) {
                $.toast('请填写合法的手机号');
                return false;
            }

         $.post("${ctx}/scenic/ticketsubmit",
            {
                username : $("#username").val(),
                userphone : $("#userphone").val(),
                buynum : $("#qty_num").val()
            },
            function(data) {
                if(data == "success"){
                    $.toast('订单提交成功');
                    window.location.href = "${ctx}/order/toordersuc";
                }else {
                    $.toast('订单提交失败');
                }

            }
            );
            /*$('#formobj')[0].submit();*/
        });


        /*点击减号优惠和运费算出来 */
        $('.number-selector-sub').click(function() {

            upDownOperation2($(this), favorablePrice, productUnit, 0);

        });
        /*点击加号数量 以及合计算出来*/
        $('.number-selector-plus').click(function() {
            upDownOperation2($(this), favorablePrice, productUnit, 0);
        });


    });


    //获取购买人信息
    function getuserinfo() {

        $.get("${ctx}/userinfo/getuserinfo",
            function(data) {
               $("#username").val(data.data.name);
               $("#userphone").val(data.data.telephone);
            }
        );
    }


    //获取产品详情
    function getticketdetail() {

        $.get("${ctx}/ticket/ticketdetails",
            function(data) {
                $("#tickettitle").html(data.data.productName);
                returnFloat(data.data.favorablePrice);
                var buynum = data.data.buyNum;
                $("#qty_num").val(buynum);
                productUnit = data.data.productUnit;
                sellpriceall = data.data.favorablePrice;

                var youhui = productUnit - sellpriceall;
                youhui  = youhui * buynum;



                $('.titaler').html('(已优惠<em class="youh">'+youhui+'</em>元)');
                $('.pieces').html(buynum);
                $('.pieces_all').html(buynum);

                var sellprice = sellpriceall * buynum ;
                returnFloat(sellprice);
                var html1 ='小计：<em class="totalpro">'+largePrice+'</em><i>'+smallPrice+'</i>'
                $('.subtotal').html(html1);
                var html2 = '合计：<em>'+largePrice+'</em><i>'+smallPrice+'</i>'
                $('.combined_total').html(html2);

            }
        );
    }

    function returnFloat(value){
        var value=Math.round(parseFloat(value)*100)/100;
        var xsd=value.toString().split(".");
        if(xsd.length==1){
            value=value.toString()+".00";
            result = value .split(".");
            largePrice = result[0];
            smallPrice = '.'+ result[1];

        }
        if(xsd.length>1){
            if(xsd[1].length<2){
                value=value.toString()+"0";
                result = value .split(".");
                largePrice = result[0];
                smallPrice = '.'+ result[1];

            }

        }

    }


</script>
</body>

</html>
