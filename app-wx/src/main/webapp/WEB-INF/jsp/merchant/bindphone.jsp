<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@include file="/inc/import.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="applicable-device" content="mobile">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

    <!--苹果专用-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="乡丽乡亲">
    <meta name="format-detection" content="telphone=yes, email=yes" />
    <title>绑定手机号</title>
    <link rel="stylesheet" type="text/css" href="/resources/css/weui.min.css" />
    <link rel="stylesheet" type="text/css" href="/resources/css/jquery-weui.css" />

    <link rel="stylesheet" type="text/css" href="/resources/css/app.css" />
</head>

<body class="bg_fafafa" ontouchstart>
<section>
    <!--	data-* 属性用于存储页面或应用程序的私有自定义数据。-->
    <form id="formyz" action="" class="am-form" data-am-validator="" data-url="/register">
        <input id="holdidentify" hidden="hidden">
        <ul class="weui-cells loginmain weui-cells_form">
            <li class="weui-cell">
                <div class="weui-cell__hd">
                    <label class="weui-label">姓名</label>
                </div>
                <div class="weui-cell__bd weui-cell_primary">
                    <input class="weui-input" type="text" name="name" value="" placeholder="请输入姓名">
                </div>
            </li>
            <li class="weui-cell weui-cell_select weui-cell_select-after">
                <div class="weui-cell__hd">
                    <label for="" class="weui-label">性别</label>
                </div>
                <div class="weui-cell__bd">
                    <select class="weui-select" name="select2">
                        <option value="1">男</option>
                        <option value="2">女</option>
                    </select>
                </div>
            </li>
            <li class="weui-cell">
                <div class="weui-cell__hd"><label for="" class="weui-label">出生日期</label></div>
                <div class="weui-cell__bd">
                    <input class="weui-input" type="date" value="" placeholder="请选择日期">
                </div>
            </li>
            <li class="weui-cell">
                <div class="weui-cell__hd">
                    <label class="weui-label">手机号码</label>
                </div>
                <div class="weui-cell__bd weui-cell_primary">
                    <input class="weui-input" type="number" name="phone" value="" pattern="[0-9]*" placeholder="请输入号码">
                </div>
            </li>
            <li class="weui-cell">
                <div class=" aside">
                    <input class="weui-input" type="number" name="phone" value="" pattern="[0-9]*" placeholder="输入验证码">
                </div>
                <div class="flower">
                    <input type="button" name="sendCodeBtn" id="sendCodeBtn"  value="获取验证码" class="weui-btn weui-btn_primary codebtn red-blue" />
                </div>

            </li>

        </ul>
        <div class="weui-btn-area">
            <button class="weui-btn weui-btn_primary  shlogin" id="shadowbtn">注&nbsp;&nbsp;册</button>
        </div>
    </form>

</section>
</body>

<script type="text/javascript" src="/resources/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-weui.js"></script>
<script type="text/javascript" src="/resources/js/fastclick.js"></script>


<script type="text/javascript">
    var timer = null;
    function myTimeout(this_){
        var second = 60;
        timer = setInterval(function(){
            second -= 1;
            if(second >0 ){
                $(this_).val(second+"秒后重新获取");
                $(this_).disabled=true; //可以被点击
                $(this_).addClass("weui-btn_disabled");//this不能被点击了，点击失效
            }else{
                clearInterval(timer);
                timer=null;
                $(this_).val("获取验证码");
                $(this_).disabled=false; //可以被点击
                $(this_).removeClass("weui-btn_disabled");//this不能被点击了，点击失效
            }
        },1000);
    }


    $(function() {
        $("body").on("click","#sendCodeBtn",function(){
            if(timer == null){
                myTimeout(this);
                getidentify();
                return ;
            }
        });


    })


    //获取验证码
    function getidentify() {
        $.get("${ctx}/userbind/getidentify",
            function(data) {
                $("#holdidentify").val(data);

            }
        );
    }
</script>

</html>