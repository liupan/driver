package com.oseasy.xlxq.service.web.security;

import com.oseasy.xlxq.service.core.security.SecurityUser;

/**
 * Created by Tandy on 2016/6/8.
 */
public interface SecurityUserRepository {
    public SecurityUser loadSecurityUser(String token);
}
