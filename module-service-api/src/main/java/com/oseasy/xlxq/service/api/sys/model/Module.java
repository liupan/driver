package com.oseasy.xlxq.service.api.sys.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.Where;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * 界面模块
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_module")
public class Module extends IdEntity {

	private  String moduleName;//模块名
	private  String parentId;//父节点标识
	private  String moduleDiscrip;//模块描述
	private  String moduleUrl;//跳转地址

	@Column(name = "module_name")
	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	@Column(name = "parent_id")
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@Column(name = "module_discrip")
	public String getModuleDiscrip() {
		return moduleDiscrip;
	}

	public void setModuleDiscrip(String moduleDiscrip) {
		this.moduleDiscrip = moduleDiscrip;
	}

	@Column(name = "module_url")
	public String getModuleUrl() {
		return moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}
}
