package com.oseasy.xlxq.service.api.video.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.video.model.VideoRoom;

public interface VideoRoomService extends BaseService<VideoRoom> {

}
