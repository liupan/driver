package com.oseasy.xlxq.service.api.video.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.video.model.VideoLib;

public interface VideoLibService extends BaseService<VideoLib> {

}
