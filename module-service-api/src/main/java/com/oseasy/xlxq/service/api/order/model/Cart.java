package com.oseasy.xlxq.service.api.order.model;

import com.oseasy.xlxq.service.api.base.IdEntity;

import com.oseasy.xlxq.service.api.tenant.model.TenantMerchant;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;
import com.oseasy.xlxq.service.api.tenant.model.TenantUser;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;


/**
 * 购物车
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_cart")
public class Cart extends IdEntity {

	private TenantProduct tenantProduct;//产品id

	private TenantMerchant tenantMerchant;//产品所属商户

	private TenantUser tenantUser;//购买人

	private String payStatus;//下单状态



	@OneToOne
	@JoinColumn(name = "product_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantProduct getTenantProduct() {
		return tenantProduct;
	}

	public void setTenantProduct(TenantProduct tenantProduct) {
		this.tenantProduct = tenantProduct;
	}

	@OneToOne
	@JoinColumn(name = "purchaser_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantMerchant getTenantMerchant() {
		return tenantMerchant;
	}

	public void setTenantMerchant(TenantMerchant tenantMerchant) {
		this.tenantMerchant = tenantMerchant;
	}


	@OneToOne
	@JoinColumn(name = "tenant_user_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantUser getTenantUser() {
		return tenantUser;
	}

	public void setTenantUser(TenantUser tenantUser) {
		this.tenantUser = tenantUser;
	}

	@Column(name = "pay_status")
	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
}
