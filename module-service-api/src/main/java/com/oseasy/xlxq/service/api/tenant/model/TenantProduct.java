package com.oseasy.xlxq.service.api.tenant.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * 产品
 * 
 *
 * 
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema="db_oe_xlxq",name = "tb_tenant_product")
@Inheritance(strategy = InheritanceType.JOINED)
public class TenantProduct extends IdEntity {

	private  String productName;//产品名

	private TenantMerchant tenantMerchant;//所属商户

	private BigDecimal productUnit;//产品单价

	private BigDecimal favorablePrice;//优惠价格

	private String titleImgUrl;//封面图片路径

	private  String productDetails;//产品详情

	private String productType;//产品类型

	private String salesVolume;//产品销量

	private String inventoryNum;//库存数量

	private  Integer recommended;//推荐广告位

	private Integer state;//上下架状态  0：下架 1：上架

	private Date shelveOnTime;//上架时间

	private Integer buyNumber;//购买人数


	@Column(name = "product_name")
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}


	@ManyToOne
	@JoinColumn(name = "merchant_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantMerchant getTenantMerchant() {
		return tenantMerchant;
	}

	public void setTenantMerchant(TenantMerchant tenantMerchant) {
		this.tenantMerchant = tenantMerchant;
	}

	@Column(name = "product_unit")
	public BigDecimal getProductUnit() {
		return productUnit;
	}

	public void setProductUnit(BigDecimal productUnit) {
		this.productUnit = productUnit;
	}


	@Column(name = "favorable_price")
	public BigDecimal getFavorablePrice() {
		return favorablePrice;
	}

	public void setFavorablePrice(BigDecimal favorablePrice) {
		this.favorablePrice = favorablePrice;
	}


	@Column(name = "product_details")
	public String getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(String productDetails) {
		this.productDetails = productDetails;
	}

	@Column(name = "product_type")
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	@Column(name = "inventory_num")
	public String getInventoryNum() {
		return inventoryNum;
	}

	public void setInventoryNum(String inventoryNum) {
		this.inventoryNum = inventoryNum;
	}

	@Column(name = "recommended")
	public Integer getRecommended() {
		return recommended;
	}

	public void setRecommended(Integer recommended) {
		this.recommended = recommended;
	}

	@Column(name = "sales_volume")
	public String getSalesVolume() {
		return salesVolume;
	}

	public void setSalesVolume(String salesVolume) {
		this.salesVolume = salesVolume;
	}

	@Column(name = "title_img_url")
	public String getTitleImgUrl() {
		return titleImgUrl;
	}

	public void setTitleImgUrl(String titleImgUrl) {
		this.titleImgUrl = titleImgUrl;
	}

	@Column(name = "state")
	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "shelve_on_time")
	public Date getShelveOnTime() {
		return shelveOnTime;
	}

	public void setShelveOnTime(Date shelveOnTime) {
		this.shelveOnTime = shelveOnTime;
	}

	@Transient
	public Integer getBuyNumber() {
		return buyNumber;
	}

	public void setBuyNumber(Integer buyNumber) {
		this.buyNumber = buyNumber;
	}
}
