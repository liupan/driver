package com.oseasy.xlxq.service.api.exceptions;

/**
 * 当session中取不到连接restApi所要的token时，抛出此异常
 * Created by liups on 2016/6/28.
 */
public class TokenMissingException extends XlxqRuntimeException {

    public TokenMissingException(String message) {
        super("0000111",message,null);
    }
}
