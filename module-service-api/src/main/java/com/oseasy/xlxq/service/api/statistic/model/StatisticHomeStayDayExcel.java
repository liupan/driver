package com.oseasy.xlxq.service.api.statistic.model;

import javax.persistence.Transient;

/**
 * 民宿日统计销量导出EXCEL VO
 * Created by heckman on 2017/6/12.
 */
public class StatisticHomeStayDayExcel {

    private String scenicName;
    private String merchantName;
    private String roomType;
    private String sellNum;
    private String sellSum;

    @Transient
    public String getScenicName() {
        return scenicName;
    }

    @Transient
    public String getMerchantName() {
        return merchantName;
    }

    @Transient
    public String getRoomType() {
        return roomType;
    }

    @Transient
    public String getSellNum() {
        return sellNum;
    }

    @Transient
    public String getSellSum() {
        return sellSum;
    }

    public void setScenicName(String scenicName) {
        this.scenicName = scenicName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public void setSellNum(String sellNum) {
        this.sellNum = sellNum;
    }

    public void setSellSum(String sellSum) {
        this.sellSum = sellSum;
    }
}
