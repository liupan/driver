package com.oseasy.xlxq.service.api.sys.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.sys.model.PageacessLog;

public interface PageacessLogService extends BaseService<PageacessLog> {

}
