package com.oseasy.xlxq.service.api.order.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.order.model.ConfigExpress;

/**
 * Created by heckman on 2017/5/26.
 */
public interface ConfigExpressService extends BaseService<ConfigExpress> {
}
