package com.oseasy.xlxq.service.api.statistic.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.statistic.model.StatisticSpecialtyDay;


public interface StatisticSpecialtyDayService extends BaseService<StatisticSpecialtyDay> {

}
