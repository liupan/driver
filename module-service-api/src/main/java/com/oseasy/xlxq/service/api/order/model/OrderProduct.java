package com.oseasy.xlxq.service.api.order.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.TenantMerchant;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;


/**
 * 订单产品信息
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_order_product")
public class OrderProduct extends IdEntity {

	private TenantProduct tenantProduct;//所属产品
	private OrderInfo orderInfo;//所属订单
	private TenantMerchant tenantMerchant;//所属商户

	private BigDecimal productPrice;//产品定价
	private BigDecimal favorablePrice;//成交售价
	private Integer purchaseNum;//购买数量


	@ManyToOne
	@JoinColumn(name = "product_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantProduct getTenantProduct() {
		return tenantProduct;
	}

	public void setTenantProduct(TenantProduct tenantProduct) {
		this.tenantProduct = tenantProduct;
	}

	@ManyToOne
	@JoinColumn(name = "order_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	@ManyToOne
	@JoinColumn(name = "merchant_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantMerchant getTenantMerchant() {
		return tenantMerchant;
	}

	public void setTenantMerchant(TenantMerchant tenantMerchant) {
		this.tenantMerchant = tenantMerchant;
	}

	@Column(name = "product_price")
	public BigDecimal getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(BigDecimal productPrice) {
		this.productPrice = productPrice;
	}

	@Column(name = "favorable_price")
	public BigDecimal getFavorablePrice() {
		return favorablePrice;
	}

	public void setFavorablePrice(BigDecimal favorablePrice) {
		this.favorablePrice = favorablePrice;
	}


	@Column(name = "purchase_num")
	public Integer getPurchaseNum() {
		return purchaseNum;
	}

	public void setPurchaseNum(Integer purchaseNum) {
		this.purchaseNum = purchaseNum;
	}

}
