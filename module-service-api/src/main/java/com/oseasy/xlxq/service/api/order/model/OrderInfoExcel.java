package com.oseasy.xlxq.service.api.order.model;

import javax.persistence.Transient;
import java.util.Date;

/**
 * Created by heckman on 2017/6/9.
 */
public class OrderInfoExcel {

    private String orderNum;
    private String merchantName;
    private String name;
    private String telephone;
    private String extTicketPop1;
    private String extHomestayPop1;
    private Date createTime;
    private String payType;
    private String orderSum;
    private String orderStatus;
    private Date lastTime;

    @Transient
    public String getOrderNum() {
        return orderNum;
    }

    @Transient
    public String getMerchantName() {
        return merchantName;
    }

    @Transient
    public String getName() {
        return name;
    }

    @Transient
    public String getTelephone() {
        return telephone;
    }

    @Transient
    public String getExtTicketPop1() {
        return extTicketPop1;
    }

    @Transient
    public String getExtHomestayPop1() {
        return extHomestayPop1;
    }

    @Transient
    public Date getCreateTime() {
        return createTime;
    }

    @Transient
    public String getPayType() {
        return payType;
    }

    @Transient
    public String getOrderSum() {
        return orderSum;
    }

    @Transient
    public String getOrderStatus() {
        return orderStatus;
    }

    @Transient
    public Date getLastTime() {
        return lastTime;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setExtTicketPop1(String extTicketPop1) {
        this.extTicketPop1 = extTicketPop1;
    }

    public void setExtHomestayPop1(String extHomestayPop1) {
        this.extHomestayPop1 = extHomestayPop1;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public void setOrderSum(String orderSum) {
        this.orderSum = orderSum;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }
}
