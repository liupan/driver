package com.oseasy.xlxq.service.api.product.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.TenantProduct;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;


/**
 * 核销
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_product_writeoff")
public class ProductWriteOff extends IdEntity {

    private TenantProduct tenantProduct;//产品
    private String productName;//产品名称
    private Date changeTime;//核销时间
    private String changemanNum;//核销人

    @ManyToOne
    @JoinColumn(name = "product_id")
    @NotFound(action = NotFoundAction.IGNORE)
    public TenantProduct getTenantProduct() {
        return tenantProduct;
    }

    public void setTenantProduct(TenantProduct tenantProduct) {
        this.tenantProduct = tenantProduct;
    }

    @Column(name="product_name")
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Column(name="change_time")
    public Date getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Date changeTime) {
        this.changeTime = changeTime;
    }

    @Column(name="changeman_num")
    public String getChangemanNum() {
        return changemanNum;
    }

    public void setChangemanNum(String changemanNum) {
        this.changemanNum = changemanNum;
    }
}
