package com.oseasy.xlxq.service.api.exceptions;

/**
 * Created by heckman on 2017/5/16.
 */
public enum ErrorConstant {

    INNER_ERROR("1001001", "内部程序错误"),
    PARAM_NULL_EXPT("1001003", "请求参数异常：参数不能为空"),
    NOT_LOGIN_EXPT("0010", "登录超时"),
    UPLOAD_MAXSIZE_LIMIT_EXCEPTION("0011","上传文件大小超出最大限制"),
    VIDEO_CHANNEL_DEL_EXPT("1001004", "删除直播频道异常：请移除频道下所有直播间后，再删除"),
    CANNOT_CANCEL_ORDER("1001005", "不能取消订单"),
    CANNOT_DELIVER_ORDER("1001006", "不能发货"),
    CANNOT_REFUND_ORDER("1001007", "不能退款");

    public static String INNER_ERROR_CODE = "1001001";
    public static String PARAM_NULL_EXPT_CODE = "1001003";
    public static String VIDEO_CHANNEL_DEL_EXPT_CODE = "1001004";
    public static String CANNOT_CANCEL_ORDER_CODE = "1001005";
    public static String CANNOT_DELIVER_ORDER_CODE = "1001006";
    public static String CANNOT_REFUND_ORDER_CODE = "1001007";

    private String errCode;
    private String errMsg;


    ErrorConstant(String errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public static String getErrMsg(String errCode) {
        for (ErrorConstant errorConstant : ErrorConstant.values()) {
            if (errCode.equals(errorConstant.getErrCode())) {
                return errorConstant.getErrMsg();
            }
        }
        return INNER_ERROR.getErrMsg();
    }
}
