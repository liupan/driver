package com.oseasy.xlxq.service.api.merchant.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.merchant.model.Merchant;

public interface MerchantService extends BaseService<Merchant> {
}
