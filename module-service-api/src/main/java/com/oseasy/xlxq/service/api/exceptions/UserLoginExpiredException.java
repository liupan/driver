package com.oseasy.xlxq.service.api.exceptions;


/**
 * Created by tandy on 17/5/24.
 * 用户登录超时异常
 */
public class UserLoginExpiredException extends XlxqRuntimeException {
    public UserLoginExpiredException() {
        super(ErrorConstant.NOT_LOGIN_EXPT.getErrCode(),ErrorConstant.NOT_LOGIN_EXPT.getErrMsg(),null);
    }


}
