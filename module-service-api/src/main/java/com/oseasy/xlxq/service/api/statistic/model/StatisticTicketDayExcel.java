package com.oseasy.xlxq.service.api.statistic.model;

import javax.persistence.Transient;

/**
 * 门票日销量统计导出excel类
 * Created by heckman on 2017/6/12.
 */
public class StatisticTicketDayExcel {

    private String scenicName;

    private String merchantName;

    private String ticketName;

    private String sellNum;

    private String sellSum;

    @Transient
    public String getScenicName() {
        return scenicName;
    }

    @Transient
    public String getMerchantName() {
        return merchantName;
    }

    @Transient
    public String getTicketName() {
        return ticketName;
    }

    @Transient
    public String getSellNum() {
        return sellNum;
    }

    @Transient
    public String getSellSum() {
        return sellSum;
    }

    public void setScenicName(String scenicName) {
        this.scenicName = scenicName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    public void setSellNum(String sellNum) {
        this.sellNum = sellNum;
    }

    public void setSellSum(String sellSum) {
        this.sellSum = sellSum;
    }
}
