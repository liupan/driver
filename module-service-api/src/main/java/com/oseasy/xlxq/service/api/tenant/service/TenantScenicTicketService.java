package com.oseasy.xlxq.service.api.tenant.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.tenant.model.TenantScenicTicket;


public interface TenantScenicTicketService extends BaseService<TenantScenicTicket> {

}
