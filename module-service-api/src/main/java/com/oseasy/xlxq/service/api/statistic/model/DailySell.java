package com.oseasy.xlxq.service.api.statistic.model;

import javax.persistence.Transient;
import java.math.BigDecimal;

/**
 * 日销售额实体
 * Created by heckman on 2017/6/13.
 */
public class DailySell {

    private String datestr;
    private String merchantName;
    private BigDecimal dailySellSum;
    private BigDecimal dailyRefundSum;
    private BigDecimal purSellSum;

    @Transient
    public String getDatestr() {
        return datestr;
    }

    public void setDatestr(String datestr) {
        this.datestr = datestr;
    }
    @Transient
    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }
    @Transient
    public BigDecimal getDailySellSum() {
        return dailySellSum;
    }

    public void setDailySellSum(BigDecimal dailySellSum) {
        this.dailySellSum = dailySellSum;
    }
    @Transient
    public BigDecimal getDailyRefundSum() {
        return dailyRefundSum;
    }

    public void setDailyRefundSum(BigDecimal dailyRefundSum) {
        this.dailyRefundSum = dailyRefundSum;
    }
    @Transient
    public BigDecimal getPurSellSum() {
        return purSellSum;
    }

    public void setPurSellSum(BigDecimal purSellSum) {
        this.purSellSum = purSellSum;
    }
}
