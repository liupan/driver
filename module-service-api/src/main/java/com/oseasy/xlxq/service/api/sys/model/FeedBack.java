package com.oseasy.xlxq.service.api.sys.model;

import com.oseasy.xlxq.service.api.base.IdEntity;
import com.oseasy.xlxq.service.api.tenant.model.TenantUser;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.util.Date;


/**
 * 反馈
 *
 */
@Entity
@Where(clause = "deleted=0")
@Table(schema = "db_oe_xlxq", name = "tb_feedback")
public class FeedBack extends IdEntity {

	//private String  backueserId;//反馈人id
	private TenantUser tenantUser;//反馈人

	private String  feedbackInfo;//反馈内容

	private String  feedbackStatus;//反馈状态  0:未回复 1:已回复

	private String  replyInfo;//回复内容

	private Date replyTime;//回复时间

	private String isReply;//是否是回复 1：是  空或其他值：

	/*@Column(name = "backueser_id")
	public String getBackueserId() {
		return backueserId;
	}

	public void setBackueserId(String backueserId) {
		this.backueserId = backueserId;
	}*/

	@ManyToOne
	@JoinColumn(name = "backueser_id")
	@NotFound(action = NotFoundAction.IGNORE)
	public TenantUser getTenantUser() {
		return tenantUser;
	}

	public void setTenantUser(TenantUser tenantUser) {
		this.tenantUser = tenantUser;
	}

	@Column(name = "feedback_info")
	public String getFeedbackInfo() {
		return feedbackInfo;
	}

	public void setFeedbackInfo(String feedbackInfo) {
		this.feedbackInfo = feedbackInfo;
	}

	@Column(name = "feedback_status")
	public String getFeedbackStatus() {
		return feedbackStatus;
	}


	public void setFeedbackStatus(String feedbackStatus) {
		this.feedbackStatus = feedbackStatus;
	}

	@Column(name = "reply_info")
	public String getReplyInfo() {
		return replyInfo;
	}

	public void setReplyInfo(String replyInfo) {
		this.replyInfo = replyInfo;
	}

	@Column(name = "reply_time")
	public Date getReplyTime() {
		return replyTime;
	}

	public void setReplyTime(Date replyTime) {
		this.replyTime = replyTime;
	}

	@Transient
	public String getIsReply() {
		return isReply;
	}

	public void setIsReply(String isReply) {
		this.isReply = isReply;
	}
}
