package com.oseasy.xlxq.service.api.exceptions;

/**
 * Created by tandy on 17/5/25.
 */
public class UploadFileException extends XlxqRuntimeException {
    public UploadFileException(Exception ex) {
        super("005500","上传文件出现异常",ex);
    }

    public UploadFileException(String msg) {
        super("005501",msg,null);
    }
}
