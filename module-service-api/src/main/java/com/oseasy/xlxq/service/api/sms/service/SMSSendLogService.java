package com.oseasy.xlxq.service.api.sms.service;

import com.oseasy.xlxq.service.api.base.BaseService;
import com.oseasy.xlxq.service.api.sms.model.SMSSendLog;
import com.oseasy.xlxq.service.core.utils.Page;

public interface SMSSendLogService extends BaseService<SMSSendLog> {


    /**
     * 根据手机号查询短信发送记录
     * @param mobile  手机号
     * @param pageNo    页号
     * @param pageSize  页大小
     */
    Page<SMSSendLog> findByMobile(String mobile, int pageNo, int pageSize);
}
